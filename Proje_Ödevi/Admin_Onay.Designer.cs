﻿namespace Proje_Ödevi
{
    partial class Admin_Onay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Data_Onaylanmamışlar = new System.Windows.Forms.DataGridView();
            this.Buton_Onayla = new System.Windows.Forms.Button();
            this.Buton_Kaydet = new System.Windows.Forms.Button();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Confirmation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Data_Onaylanmamışlar)).BeginInit();
            this.SuspendLayout();
            // 
            // Data_Onaylanmamışlar
            // 
            this.Data_Onaylanmamışlar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Data_Onaylanmamışlar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this._Name,
            this.Adress,
            this.Username,
            this.Email,
            this.Confirmation,
            this.Password});
            this.Data_Onaylanmamışlar.Location = new System.Drawing.Point(13, 13);
            this.Data_Onaylanmamışlar.Name = "Data_Onaylanmamışlar";
            this.Data_Onaylanmamışlar.Size = new System.Drawing.Size(644, 272);
            this.Data_Onaylanmamışlar.TabIndex = 0;
            this.Data_Onaylanmamışlar.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Data_Onaylanmamışlar_CellMouseUp);
            // 
            // Buton_Onayla
            // 
            this.Buton_Onayla.Location = new System.Drawing.Point(663, 12);
            this.Buton_Onayla.Name = "Buton_Onayla";
            this.Buton_Onayla.Size = new System.Drawing.Size(96, 34);
            this.Buton_Onayla.TabIndex = 1;
            this.Buton_Onayla.Text = "Seçileni Onayla";
            this.Buton_Onayla.UseVisualStyleBackColor = true;
            this.Buton_Onayla.Click += new System.EventHandler(this.Buton_Onayla_Click);
            // 
            // Buton_Kaydet
            // 
            this.Buton_Kaydet.Location = new System.Drawing.Point(663, 53);
            this.Buton_Kaydet.Name = "Buton_Kaydet";
            this.Buton_Kaydet.Size = new System.Drawing.Size(96, 34);
            this.Buton_Kaydet.TabIndex = 2;
            this.Buton_Kaydet.Text = "Kaydet ve Çık";
            this.Buton_Kaydet.UseVisualStyleBackColor = true;
            this.Buton_Kaydet.Click += new System.EventHandler(this.Buton_Kaydet_Click);
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            // 
            // _Name
            // 
            this._Name.HeaderText = "İsim";
            this._Name.Name = "_Name";
            // 
            // Adress
            // 
            this.Adress.HeaderText = "Adres";
            this.Adress.Name = "Adress";
            // 
            // Username
            // 
            this.Username.HeaderText = "Kullanıcı Adı";
            this.Username.Name = "Username";
            // 
            // Email
            // 
            this.Email.HeaderText = "Eposta";
            this.Email.Name = "Email";
            // 
            // Confirmation
            // 
            this.Confirmation.HeaderText = "Onay Durumu";
            this.Confirmation.Name = "Confirmation";
            // 
            // Password
            // 
            this.Password.HeaderText = "Şifre";
            this.Password.Name = "Password";
            this.Password.Visible = false;
            // 
            // Admin_Onay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 297);
            this.Controls.Add(this.Buton_Kaydet);
            this.Controls.Add(this.Buton_Onayla);
            this.Controls.Add(this.Data_Onaylanmamışlar);
            this.Name = "Admin_Onay";
            this.Text = "Admin Onay";
            ((System.ComponentModel.ISupportInitialize)(this.Data_Onaylanmamışlar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Data_Onaylanmamışlar;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.Button Buton_Onayla;
        private System.Windows.Forms.Button Buton_Kaydet;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Confirmation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Password;
    }
}