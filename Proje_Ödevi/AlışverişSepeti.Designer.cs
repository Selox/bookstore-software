﻿namespace Proje_Ödevi
{
    partial class AlışverişSepeti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Buton_Alışveriş_Tamamla = new System.Windows.Forms.Button();
            this.Page = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Publisher = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Book_DataGrid = new System.Windows.Forms.DataGridView();
            this.Magazine_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Issue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Magazine_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Magazine_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Magazine_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Magazine_DataGrid = new System.Windows.Forms.DataGridView();
            this.Music_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Singer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Music_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Music_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Music_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Music_DataGrid = new System.Windows.Forms.DataGridView();
            this.Menü_Alışveriş_Sepeti = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Add_to_Basket = new System.Windows.Forms.ToolStripMenuItem();
            this.Remove_From_Basket = new System.Windows.Forms.ToolStripMenuItem();
            this.Clear_Selected = new System.Windows.Forms.ToolStripMenuItem();
            this.Data_Ürünler = new System.Windows.Forms.DataGridView();
            this.Product_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resim_Ürün = new System.Windows.Forms.PictureBox();
            this.Buton_İptal_Et = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Book_DataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Magazine_DataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Music_DataGrid)).BeginInit();
            this.Menü_Alışveriş_Sepeti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Data_Ürünler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Ürün)).BeginInit();
            this.SuspendLayout();
            // 
            // Buton_Alışveriş_Tamamla
            // 
            this.Buton_Alışveriş_Tamamla.Location = new System.Drawing.Point(12, 373);
            this.Buton_Alışveriş_Tamamla.Name = "Buton_Alışveriş_Tamamla";
            this.Buton_Alışveriş_Tamamla.Size = new System.Drawing.Size(111, 21);
            this.Buton_Alışveriş_Tamamla.TabIndex = 9;
            this.Buton_Alışveriş_Tamamla.Text = "Alışverişi Tamamla";
            this.Buton_Alışveriş_Tamamla.UseVisualStyleBackColor = true;
            this.Buton_Alışveriş_Tamamla.Click += new System.EventHandler(this.Buton_Alışveriş_Tamamla_Click);
            // 
            // Page
            // 
            this.Page.HeaderText = "Sayfa";
            this.Page.Name = "Page";
            this.Page.ReadOnly = true;
            this.Page.Width = 50;
            // 
            // Publisher
            // 
            this.Publisher.HeaderText = "Yayıncı";
            this.Publisher.Name = "Publisher";
            this.Publisher.ReadOnly = true;
            this.Publisher.Width = 110;
            // 
            // Author
            // 
            this.Author.HeaderText = "Yazar";
            this.Author.Name = "Author";
            this.Author.ReadOnly = true;
            this.Author.Width = 110;
            // 
            // ISBN
            // 
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            this.ISBN.ReadOnly = true;
            // 
            // Book_Price
            // 
            this.Book_Price.HeaderText = "Fiyat";
            this.Book_Price.Name = "Book_Price";
            this.Book_Price.ReadOnly = true;
            this.Book_Price.Width = 52;
            // 
            // Book_Name
            // 
            this.Book_Name.HeaderText = "İsim";
            this.Book_Name.Name = "Book_Name";
            this.Book_Name.ReadOnly = true;
            // 
            // Book_ID
            // 
            this.Book_ID.HeaderText = "ID";
            this.Book_ID.Name = "Book_ID";
            this.Book_ID.ReadOnly = true;
            this.Book_ID.Width = 60;
            // 
            // Book_DataGrid
            // 
            this.Book_DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Book_DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Book_ID,
            this.Book_Name,
            this.Book_Price,
            this.ISBN,
            this.Author,
            this.Publisher,
            this.Page});
            this.Book_DataGrid.Location = new System.Drawing.Point(12, 12);
            this.Book_DataGrid.Name = "Book_DataGrid";
            this.Book_DataGrid.ReadOnly = true;
            this.Book_DataGrid.Size = new System.Drawing.Size(625, 114);
            this.Book_DataGrid.TabIndex = 15;
            this.Book_DataGrid.Tag = "";
            this.Book_DataGrid.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Book_DataGrid_CellMouseUp);
            this.Book_DataGrid.SelectionChanged += new System.EventHandler(this.Book_DataGrid_SelectionChanged);
            // 
            // Magazine_Type
            // 
            this.Magazine_Type.HeaderText = "Tür";
            this.Magazine_Type.Name = "Magazine_Type";
            this.Magazine_Type.ReadOnly = true;
            // 
            // Issue
            // 
            this.Issue.HeaderText = "Sayı";
            this.Issue.Name = "Issue";
            this.Issue.ReadOnly = true;
            // 
            // Magazine_Price
            // 
            this.Magazine_Price.HeaderText = "Fiyat";
            this.Magazine_Price.Name = "Magazine_Price";
            this.Magazine_Price.ReadOnly = true;
            // 
            // Magazine_Name
            // 
            this.Magazine_Name.HeaderText = "İsim";
            this.Magazine_Name.Name = "Magazine_Name";
            this.Magazine_Name.ReadOnly = true;
            // 
            // Magazine_ID
            // 
            this.Magazine_ID.HeaderText = "ID";
            this.Magazine_ID.Name = "Magazine_ID";
            this.Magazine_ID.ReadOnly = true;
            // 
            // Magazine_DataGrid
            // 
            this.Magazine_DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Magazine_DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Magazine_ID,
            this.Magazine_Name,
            this.Magazine_Price,
            this.Issue,
            this.Magazine_Type});
            this.Magazine_DataGrid.Location = new System.Drawing.Point(12, 132);
            this.Magazine_DataGrid.Name = "Magazine_DataGrid";
            this.Magazine_DataGrid.ReadOnly = true;
            this.Magazine_DataGrid.Size = new System.Drawing.Size(625, 114);
            this.Magazine_DataGrid.TabIndex = 16;
            this.Magazine_DataGrid.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Magazine_DataGrid_CellMouseUp);
            this.Magazine_DataGrid.SelectionChanged += new System.EventHandler(this.Magazine_DataGrid_SelectionChanged);
            // 
            // Music_Type
            // 
            this.Music_Type.HeaderText = "Tür";
            this.Music_Type.Name = "Music_Type";
            this.Music_Type.ReadOnly = true;
            // 
            // Singer
            // 
            this.Singer.HeaderText = "Şarkıcı";
            this.Singer.Name = "Singer";
            this.Singer.ReadOnly = true;
            // 
            // Music_Price
            // 
            this.Music_Price.HeaderText = "Fiyat";
            this.Music_Price.Name = "Music_Price";
            this.Music_Price.ReadOnly = true;
            // 
            // Music_Name
            // 
            this.Music_Name.HeaderText = "İsim";
            this.Music_Name.Name = "Music_Name";
            this.Music_Name.ReadOnly = true;
            // 
            // Music_ID
            // 
            this.Music_ID.HeaderText = "ID";
            this.Music_ID.Name = "Music_ID";
            this.Music_ID.ReadOnly = true;
            // 
            // Music_DataGrid
            // 
            this.Music_DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Music_DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Music_ID,
            this.Music_Name,
            this.Music_Price,
            this.Singer,
            this.Music_Type});
            this.Music_DataGrid.Location = new System.Drawing.Point(12, 252);
            this.Music_DataGrid.Name = "Music_DataGrid";
            this.Music_DataGrid.ReadOnly = true;
            this.Music_DataGrid.Size = new System.Drawing.Size(625, 114);
            this.Music_DataGrid.TabIndex = 17;
            this.Music_DataGrid.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Music_DataGrid_CellMouseUp);
            this.Music_DataGrid.SelectionChanged += new System.EventHandler(this.Music_DataGrid_SelectionChanged);
            // 
            // Menü_Alışveriş_Sepeti
            // 
            this.Menü_Alışveriş_Sepeti.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Add_to_Basket,
            this.Remove_From_Basket,
            this.Clear_Selected});
            this.Menü_Alışveriş_Sepeti.Name = "contextMenuStrip1";
            this.Menü_Alışveriş_Sepeti.Size = new System.Drawing.Size(171, 70);
            // 
            // Add_to_Basket
            // 
            this.Add_to_Basket.Name = "Add_to_Basket";
            this.Add_to_Basket.Size = new System.Drawing.Size(170, 22);
            this.Add_to_Basket.Text = "Ürünü Ekle";
            this.Add_to_Basket.Click += new System.EventHandler(this.Add_to_Basket_Click);
            // 
            // Remove_From_Basket
            // 
            this.Remove_From_Basket.Name = "Remove_From_Basket";
            this.Remove_From_Basket.Size = new System.Drawing.Size(170, 22);
            this.Remove_From_Basket.Text = "Ürünü Sil";
            this.Remove_From_Basket.Click += new System.EventHandler(this.Remove_From_Basket_Click);
            // 
            // Clear_Selected
            // 
            this.Clear_Selected.Name = "Clear_Selected";
            this.Clear_Selected.Size = new System.Drawing.Size(170, 22);
            this.Clear_Selected.Text = "Seçilenleri Temizle";
            this.Clear_Selected.Click += new System.EventHandler(this.Clear_Selected_Click);
            // 
            // Data_Ürünler
            // 
            this.Data_Ürünler.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Data_Ürünler.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Product_ID,
            this.Product_Name,
            this.Product_Price,
            this.Quantity});
            this.Data_Ürünler.Location = new System.Drawing.Point(643, 213);
            this.Data_Ürünler.Name = "Data_Ürünler";
            this.Data_Ürünler.Size = new System.Drawing.Size(239, 190);
            this.Data_Ürünler.TabIndex = 18;
            this.Data_Ürünler.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Data_Ürünler_CellMouseUp);
            // 
            // Product_ID
            // 
            this.Product_ID.HeaderText = "ID";
            this.Product_ID.Name = "Product_ID";
            this.Product_ID.Width = 45;
            // 
            // Product_Name
            // 
            this.Product_Name.HeaderText = "İsim";
            this.Product_Name.Name = "Product_Name";
            this.Product_Name.Width = 65;
            // 
            // Product_Price
            // 
            this.Product_Price.HeaderText = "Fiyat";
            this.Product_Price.Name = "Product_Price";
            this.Product_Price.Width = 45;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Miktar";
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 40;
            // 
            // Resim_Ürün
            // 
            this.Resim_Ürün.Location = new System.Drawing.Point(643, 12);
            this.Resim_Ürün.Name = "Resim_Ürün";
            this.Resim_Ürün.Size = new System.Drawing.Size(239, 195);
            this.Resim_Ürün.TabIndex = 19;
            this.Resim_Ürün.TabStop = false;
            // 
            // Buton_İptal_Et
            // 
            this.Buton_İptal_Et.Location = new System.Drawing.Point(129, 372);
            this.Buton_İptal_Et.Name = "Buton_İptal_Et";
            this.Buton_İptal_Et.Size = new System.Drawing.Size(111, 21);
            this.Buton_İptal_Et.TabIndex = 20;
            this.Buton_İptal_Et.Text = "Alışverişi İptal Et";
            this.Buton_İptal_Et.UseVisualStyleBackColor = true;
            this.Buton_İptal_Et.Click += new System.EventHandler(this.Buton_İptal_Et_Click);
            // 
            // AlışverişSepeti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 406);
            this.Controls.Add(this.Buton_İptal_Et);
            this.Controls.Add(this.Resim_Ürün);
            this.Controls.Add(this.Data_Ürünler);
            this.Controls.Add(this.Music_DataGrid);
            this.Controls.Add(this.Magazine_DataGrid);
            this.Controls.Add(this.Book_DataGrid);
            this.Controls.Add(this.Buton_Alışveriş_Tamamla);
            this.Name = "AlışverişSepeti";
            this.Text = "AlışverişSepeti";
            ((System.ComponentModel.ISupportInitialize)(this.Book_DataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Magazine_DataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Music_DataGrid)).EndInit();
            this.Menü_Alışveriş_Sepeti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Data_Ürünler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Ürün)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Buton_Alışveriş_Tamamla;
        private System.Windows.Forms.DataGridViewTextBoxColumn Page;
        private System.Windows.Forms.DataGridViewTextBoxColumn Publisher;
        private System.Windows.Forms.DataGridViewTextBoxColumn Author;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Book_ID;
        private System.Windows.Forms.DataGridView Book_DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Magazine_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Issue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Magazine_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Magazine_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Magazine_ID;
        private System.Windows.Forms.DataGridView Magazine_DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Music_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Singer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Music_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Music_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Music_ID;
        private System.Windows.Forms.DataGridView Music_DataGrid;
        private System.Windows.Forms.ContextMenuStrip Menü_Alışveriş_Sepeti;
        private System.Windows.Forms.ToolStripMenuItem Add_to_Basket;
        private System.Windows.Forms.ToolStripMenuItem Remove_From_Basket;
        private System.Windows.Forms.DataGridView Data_Ürünler;
        private System.Windows.Forms.ToolStripMenuItem Clear_Selected;
        private System.Windows.Forms.PictureBox Resim_Ürün;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.Button Buton_İptal_Et;
    }
}