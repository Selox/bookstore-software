﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje_Ödevi
{
    /** 
     * \brief MusicCD sınıfı.
     *  Sistemdeki müzik disklerinin oluşturulması için gereken sınıftır.
     *  @param singer şarkıcı ismini tutan parametre
     *  @param type müzik tipini tutan parametre
     *  @param picture müzik resmini tutan parametre
   */
    class MusicCD : Product
    {
        public string singer { get; set; }
        public string type { get; set; }
        public string picture { get; set; }
    }
}
