﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Proje_Ödevi
{
    public partial class Müşteri_Onay_Formu : Form
    {
        List<Customer> Müşteriler = new List<Customer>();

        public Müşteri_Onay_Formu()
        {
            InitializeComponent();
        }

        private void Buton_KaydetveÇık_Click(object sender, EventArgs e)
        {

        }
        int rowIndex = 0;

        private void Data_Müşteriler_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Data_Müşteriler.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Data_Müşteriler.CurrentCell = this.Data_Müşteriler.Rows[e.RowIndex].Cells[1];
                this.Menü_Müşteri_Onay.Show(this.Data_Müşteriler, e.Location);
                Menü_Müşteri_Onay.Show(Cursor.Position);
            }
        }

        private void Customer_Rejection_Click(object sender, EventArgs e)
        {


            this.Data_Müşteriler.Rows.RemoveAt(this.rowIndex);



        }
        private void Customer_Confirmation_Click(object sender, EventArgs e)
        {
            this.Data_Müşteriler.Rows.RemoveAt(this.rowIndex);
        }

        private void addCustomer()
        {
            string Path = "";
            Path = @".\customer.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        if (elements[6] == "true")
                        {
                            Müşteriler.Add(new Customer
                            {
                                CustomerID = Int32.Parse(elements[0]),
                                Name = elements[1],
                                Adress = elements[2],
                                Username = elements[3],
                                Email = elements[4],
                                Password = Int32.Parse(elements[5])

                            });
                        }
                    }
                }
            }

        }

    }
}
