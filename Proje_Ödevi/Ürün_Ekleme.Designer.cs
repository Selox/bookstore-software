﻿namespace Proje_Ödevi
{
    partial class Ürün_Ekleme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Text_Tanım_Ürün = new System.Windows.Forms.TextBox();
            this.Label_Tanım_Ürün = new System.Windows.Forms.Label();
            this.Buton_Resim_Yükle = new System.Windows.Forms.Button();
            this.Buton_Kayıt_Tamamla = new System.Windows.Forms.Button();
            this.Text_Tür_Ürün = new System.Windows.Forms.TextBox();
            this.Text_Fiyat_Ürün = new System.Windows.Forms.TextBox();
            this.Text_İsim_Ürün = new System.Windows.Forms.TextBox();
            this.Resim_Kayıt = new System.Windows.Forms.PictureBox();
            this.Label_Tür_Ürün = new System.Windows.Forms.Label();
            this.Label_Fiyat_Ürün = new System.Windows.Forms.Label();
            this.Label_İsim_Ürün = new System.Windows.Forms.Label();
            this.Label_Yayıncı_Ürün = new System.Windows.Forms.Label();
            this.Text_Yayıncı_Ürün = new System.Windows.Forms.TextBox();
            this.Label_SayfaSayısı_Ürün = new System.Windows.Forms.Label();
            this.Text_Sayfa_Ürün = new System.Windows.Forms.TextBox();
            this.Text_Resim = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Kayıt)).BeginInit();
            this.SuspendLayout();
            // 
            // Text_Tanım_Ürün
            // 
            this.Text_Tanım_Ürün.Location = new System.Drawing.Point(242, 67);
            this.Text_Tanım_Ürün.Name = "Text_Tanım_Ürün";
            this.Text_Tanım_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_Tanım_Ürün.TabIndex = 64;
            // 
            // Label_Tanım_Ürün
            // 
            this.Label_Tanım_Ürün.AutoSize = true;
            this.Label_Tanım_Ürün.Location = new System.Drawing.Point(156, 71);
            this.Label_Tanım_Ürün.Name = "Label_Tanım_Ürün";
            this.Label_Tanım_Ürün.Size = new System.Drawing.Size(32, 13);
            this.Label_Tanım_Ürün.TabIndex = 63;
            this.Label_Tanım_Ürün.Text = "ISBN";
            // 
            // Buton_Resim_Yükle
            // 
            this.Buton_Resim_Yükle.Location = new System.Drawing.Point(13, 171);
            this.Buton_Resim_Yükle.Name = "Buton_Resim_Yükle";
            this.Buton_Resim_Yükle.Size = new System.Drawing.Size(119, 23);
            this.Buton_Resim_Yükle.TabIndex = 62;
            this.Buton_Resim_Yükle.Text = "Resim Yükle";
            this.Buton_Resim_Yükle.UseVisualStyleBackColor = true;
            this.Buton_Resim_Yükle.Click += new System.EventHandler(this.Buton_Resim_Yükle_Click);
            // 
            // Buton_Kayıt_Tamamla
            // 
            this.Buton_Kayıt_Tamamla.Location = new System.Drawing.Point(242, 199);
            this.Buton_Kayıt_Tamamla.Name = "Buton_Kayıt_Tamamla";
            this.Buton_Kayıt_Tamamla.Size = new System.Drawing.Size(100, 25);
            this.Buton_Kayıt_Tamamla.TabIndex = 61;
            this.Buton_Kayıt_Tamamla.Text = "Tamamla";
            this.Buton_Kayıt_Tamamla.UseVisualStyleBackColor = true;
            this.Buton_Kayıt_Tamamla.Click += new System.EventHandler(this.Buton_Kayıt_Tamamla_Click);
            // 
            // Text_Tür_Ürün
            // 
            this.Text_Tür_Ürün.Location = new System.Drawing.Point(242, 94);
            this.Text_Tür_Ürün.Name = "Text_Tür_Ürün";
            this.Text_Tür_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_Tür_Ürün.TabIndex = 55;
            // 
            // Text_Fiyat_Ürün
            // 
            this.Text_Fiyat_Ürün.Location = new System.Drawing.Point(242, 41);
            this.Text_Fiyat_Ürün.Name = "Text_Fiyat_Ürün";
            this.Text_Fiyat_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_Fiyat_Ürün.TabIndex = 54;
            // 
            // Text_İsim_Ürün
            // 
            this.Text_İsim_Ürün.Location = new System.Drawing.Point(242, 15);
            this.Text_İsim_Ürün.Name = "Text_İsim_Ürün";
            this.Text_İsim_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_İsim_Ürün.TabIndex = 53;
            // 
            // Resim_Kayıt
            // 
            this.Resim_Kayıt.Location = new System.Drawing.Point(12, 12);
            this.Resim_Kayıt.Name = "Resim_Kayıt";
            this.Resim_Kayıt.Size = new System.Drawing.Size(120, 150);
            this.Resim_Kayıt.TabIndex = 52;
            this.Resim_Kayıt.TabStop = false;
            // 
            // Label_Tür_Ürün
            // 
            this.Label_Tür_Ürün.AutoSize = true;
            this.Label_Tür_Ürün.Location = new System.Drawing.Point(156, 97);
            this.Label_Tür_Ürün.Name = "Label_Tür_Ürün";
            this.Label_Tür_Ürün.Size = new System.Drawing.Size(23, 13);
            this.Label_Tür_Ürün.TabIndex = 50;
            this.Label_Tür_Ürün.Text = "Tür";
            // 
            // Label_Fiyat_Ürün
            // 
            this.Label_Fiyat_Ürün.AutoSize = true;
            this.Label_Fiyat_Ürün.Location = new System.Drawing.Point(156, 45);
            this.Label_Fiyat_Ürün.Name = "Label_Fiyat_Ürün";
            this.Label_Fiyat_Ürün.Size = new System.Drawing.Size(31, 13);
            this.Label_Fiyat_Ürün.TabIndex = 49;
            this.Label_Fiyat_Ürün.Text = "Fiyatı";
            // 
            // Label_İsim_Ürün
            // 
            this.Label_İsim_Ürün.AutoSize = true;
            this.Label_İsim_Ürün.Location = new System.Drawing.Point(157, 20);
            this.Label_İsim_Ürün.Name = "Label_İsim_Ürün";
            this.Label_İsim_Ürün.Size = new System.Drawing.Size(48, 13);
            this.Label_İsim_Ürün.TabIndex = 48;
            this.Label_İsim_Ürün.Text = "Ürün Adı";
            // 
            // Label_Yayıncı_Ürün
            // 
            this.Label_Yayıncı_Ürün.AutoSize = true;
            this.Label_Yayıncı_Ürün.Location = new System.Drawing.Point(156, 124);
            this.Label_Yayıncı_Ürün.Name = "Label_Yayıncı_Ürün";
            this.Label_Yayıncı_Ürün.Size = new System.Drawing.Size(41, 13);
            this.Label_Yayıncı_Ürün.TabIndex = 51;
            this.Label_Yayıncı_Ürün.Text = "Yayıncı";
            this.Label_Yayıncı_Ürün.Visible = false;
            // 
            // Text_Yayıncı_Ürün
            // 
            this.Text_Yayıncı_Ürün.Location = new System.Drawing.Point(242, 121);
            this.Text_Yayıncı_Ürün.Name = "Text_Yayıncı_Ürün";
            this.Text_Yayıncı_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_Yayıncı_Ürün.TabIndex = 56;
            this.Text_Yayıncı_Ürün.Visible = false;
            // 
            // Label_SayfaSayısı_Ürün
            // 
            this.Label_SayfaSayısı_Ürün.AutoSize = true;
            this.Label_SayfaSayısı_Ürün.Location = new System.Drawing.Point(156, 150);
            this.Label_SayfaSayısı_Ürün.Name = "Label_SayfaSayısı_Ürün";
            this.Label_SayfaSayısı_Ürün.Size = new System.Drawing.Size(64, 13);
            this.Label_SayfaSayısı_Ürün.TabIndex = 57;
            this.Label_SayfaSayısı_Ürün.Text = "Sayfa Sayısı";
            this.Label_SayfaSayısı_Ürün.Visible = false;
            // 
            // Text_Sayfa_Ürün
            // 
            this.Text_Sayfa_Ürün.Location = new System.Drawing.Point(242, 147);
            this.Text_Sayfa_Ürün.Name = "Text_Sayfa_Ürün";
            this.Text_Sayfa_Ürün.Size = new System.Drawing.Size(100, 20);
            this.Text_Sayfa_Ürün.TabIndex = 58;
            this.Text_Sayfa_Ürün.Visible = false;
            // 
            // Text_Resim
            // 
            this.Text_Resim.Location = new System.Drawing.Point(159, 173);
            this.Text_Resim.Name = "Text_Resim";
            this.Text_Resim.Size = new System.Drawing.Size(183, 20);
            this.Text_Resim.TabIndex = 65;
            // 
            // Ürün_Ekleme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 234);
            this.Controls.Add(this.Text_Resim);
            this.Controls.Add(this.Text_Tanım_Ürün);
            this.Controls.Add(this.Label_Tanım_Ürün);
            this.Controls.Add(this.Buton_Resim_Yükle);
            this.Controls.Add(this.Buton_Kayıt_Tamamla);
            this.Controls.Add(this.Text_Sayfa_Ürün);
            this.Controls.Add(this.Label_SayfaSayısı_Ürün);
            this.Controls.Add(this.Text_Yayıncı_Ürün);
            this.Controls.Add(this.Text_Tür_Ürün);
            this.Controls.Add(this.Text_Fiyat_Ürün);
            this.Controls.Add(this.Text_İsim_Ürün);
            this.Controls.Add(this.Resim_Kayıt);
            this.Controls.Add(this.Label_Yayıncı_Ürün);
            this.Controls.Add(this.Label_Tür_Ürün);
            this.Controls.Add(this.Label_Fiyat_Ürün);
            this.Controls.Add(this.Label_İsim_Ürün);
            this.Name = "Ürün_Ekleme";
            this.Text = "Ürün_Ekleme";
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Kayıt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Text_Tanım_Ürün;
        private System.Windows.Forms.Label Label_Tanım_Ürün;
        private System.Windows.Forms.Button Buton_Resim_Yükle;
        private System.Windows.Forms.Button Buton_Kayıt_Tamamla;
        private System.Windows.Forms.TextBox Text_Tür_Ürün;
        private System.Windows.Forms.TextBox Text_Fiyat_Ürün;
        private System.Windows.Forms.TextBox Text_İsim_Ürün;
        private System.Windows.Forms.PictureBox Resim_Kayıt;
        private System.Windows.Forms.Label Label_Tür_Ürün;
        private System.Windows.Forms.Label Label_Fiyat_Ürün;
        private System.Windows.Forms.Label Label_İsim_Ürün;
        private System.Windows.Forms.Label Label_Yayıncı_Ürün;
        private System.Windows.Forms.TextBox Text_Yayıncı_Ürün;
        private System.Windows.Forms.Label Label_SayfaSayısı_Ürün;
        private System.Windows.Forms.TextBox Text_Sayfa_Ürün;
        private System.Windows.Forms.TextBox Text_Resim;
    }
}