﻿namespace Proje_Ödevi
{
    partial class Admin_Formu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_İsim = new System.Windows.Forms.Label();
            this.Label_SonGiriş = new System.Windows.Forms.Label();
            this.Label_Rütbe = new System.Windows.Forms.Label();
            this.Label_Nick = new System.Windows.Forms.Label();
            this.Label_Admin_Nick = new System.Windows.Forms.Label();
            this.Label_Admin_Rütbe = new System.Windows.Forms.Label();
            this.Label_Admin_SonGiriş = new System.Windows.Forms.Label();
            this.Label_Admin_İsim = new System.Windows.Forms.Label();
            this.Buton_Müşteri_Onay = new System.Windows.Forms.Button();
            this.Label_Müşteri_Menü = new System.Windows.Forms.Label();
            this.Buton_Müşteri_Göster = new System.Windows.Forms.Button();
            this.Buton_Kitap_Ekle = new System.Windows.Forms.Button();
            this.Buton_Ürün_Göster = new System.Windows.Forms.Button();
            this.Label_Ürün_Menüsü = new System.Windows.Forms.Label();
            this.Resim_Admin = new System.Windows.Forms.PictureBox();
            this.Label_Eposta = new System.Windows.Forms.Label();
            this.Label_Admin_Eposta = new System.Windows.Forms.Label();
            this.Label_Adres = new System.Windows.Forms.Label();
            this.Label_Admin_Adres = new System.Windows.Forms.Label();
            this.Buton_Müzik_Ekle = new System.Windows.Forms.Button();
            this.Buton_Dergi_Ekle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Admin)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_İsim
            // 
            this.Label_İsim.AutoSize = true;
            this.Label_İsim.Location = new System.Drawing.Point(167, 13);
            this.Label_İsim.Name = "Label_İsim";
            this.Label_İsim.Size = new System.Drawing.Size(20, 13);
            this.Label_İsim.TabIndex = 1;
            this.Label_İsim.Text = "Ad";
            // 
            // Label_SonGiriş
            // 
            this.Label_SonGiriş.AutoSize = true;
            this.Label_SonGiriş.Location = new System.Drawing.Point(167, 96);
            this.Label_SonGiriş.Name = "Label_SonGiriş";
            this.Label_SonGiriş.Size = new System.Drawing.Size(49, 13);
            this.Label_SonGiriş.TabIndex = 3;
            this.Label_SonGiriş.Text = "Son Giriş";
            // 
            // Label_Rütbe
            // 
            this.Label_Rütbe.AutoSize = true;
            this.Label_Rütbe.Location = new System.Drawing.Point(167, 126);
            this.Label_Rütbe.Name = "Label_Rütbe";
            this.Label_Rütbe.Size = new System.Drawing.Size(36, 13);
            this.Label_Rütbe.TabIndex = 4;
            this.Label_Rütbe.Text = "Rütbe";
            // 
            // Label_Nick
            // 
            this.Label_Nick.AutoSize = true;
            this.Label_Nick.Location = new System.Drawing.Point(167, 68);
            this.Label_Nick.Name = "Label_Nick";
            this.Label_Nick.Size = new System.Drawing.Size(29, 13);
            this.Label_Nick.TabIndex = 5;
            this.Label_Nick.Text = "Nick";
            // 
            // Label_Admin_Nick
            // 
            this.Label_Admin_Nick.AutoSize = true;
            this.Label_Admin_Nick.Location = new System.Drawing.Point(245, 68);
            this.Label_Admin_Nick.Name = "Label_Admin_Nick";
            this.Label_Admin_Nick.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_Nick.TabIndex = 10;
            // 
            // Label_Admin_Rütbe
            // 
            this.Label_Admin_Rütbe.AutoSize = true;
            this.Label_Admin_Rütbe.Location = new System.Drawing.Point(245, 126);
            this.Label_Admin_Rütbe.Name = "Label_Admin_Rütbe";
            this.Label_Admin_Rütbe.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_Rütbe.TabIndex = 9;
            // 
            // Label_Admin_SonGiriş
            // 
            this.Label_Admin_SonGiriş.AutoSize = true;
            this.Label_Admin_SonGiriş.Location = new System.Drawing.Point(245, 96);
            this.Label_Admin_SonGiriş.Name = "Label_Admin_SonGiriş";
            this.Label_Admin_SonGiriş.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_SonGiriş.TabIndex = 8;
            // 
            // Label_Admin_İsim
            // 
            this.Label_Admin_İsim.AutoSize = true;
            this.Label_Admin_İsim.Location = new System.Drawing.Point(245, 13);
            this.Label_Admin_İsim.Name = "Label_Admin_İsim";
            this.Label_Admin_İsim.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_İsim.TabIndex = 6;
            // 
            // Buton_Müşteri_Onay
            // 
            this.Buton_Müşteri_Onay.Location = new System.Drawing.Point(389, 58);
            this.Buton_Müşteri_Onay.Name = "Buton_Müşteri_Onay";
            this.Buton_Müşteri_Onay.Size = new System.Drawing.Size(153, 23);
            this.Buton_Müşteri_Onay.TabIndex = 11;
            this.Buton_Müşteri_Onay.Text = "Müşteri Kaydı Onayla";
            this.Buton_Müşteri_Onay.UseVisualStyleBackColor = true;
            this.Buton_Müşteri_Onay.Click += new System.EventHandler(this.Buton_Müşteri_Onay_Click);
            // 
            // Label_Müşteri_Menü
            // 
            this.Label_Müşteri_Menü.AutoSize = true;
            this.Label_Müşteri_Menü.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label_Müşteri_Menü.Location = new System.Drawing.Point(378, 13);
            this.Label_Müşteri_Menü.Name = "Label_Müşteri_Menü";
            this.Label_Müşteri_Menü.Size = new System.Drawing.Size(183, 29);
            this.Label_Müşteri_Menü.TabIndex = 12;
            this.Label_Müşteri_Menü.Text = "Müşteri Menüsü";
            // 
            // Buton_Müşteri_Göster
            // 
            this.Buton_Müşteri_Göster.Location = new System.Drawing.Point(389, 86);
            this.Buton_Müşteri_Göster.Name = "Buton_Müşteri_Göster";
            this.Buton_Müşteri_Göster.Size = new System.Drawing.Size(153, 23);
            this.Buton_Müşteri_Göster.TabIndex = 13;
            this.Buton_Müşteri_Göster.Text = "Müşterileri Görüntüle";
            this.Buton_Müşteri_Göster.UseVisualStyleBackColor = true;
            this.Buton_Müşteri_Göster.Click += new System.EventHandler(this.Buton_Müşteri_Göster_Click);
            // 
            // Buton_Kitap_Ekle
            // 
            this.Buton_Kitap_Ekle.Location = new System.Drawing.Point(600, 86);
            this.Buton_Kitap_Ekle.Name = "Buton_Kitap_Ekle";
            this.Buton_Kitap_Ekle.Size = new System.Drawing.Size(139, 23);
            this.Buton_Kitap_Ekle.TabIndex = 18;
            this.Buton_Kitap_Ekle.Text = "Kitap Ekle";
            this.Buton_Kitap_Ekle.UseVisualStyleBackColor = true;
            this.Buton_Kitap_Ekle.Click += new System.EventHandler(this.Buton_Kitap_Ekle_Click);
            // 
            // Buton_Ürün_Göster
            // 
            this.Buton_Ürün_Göster.Location = new System.Drawing.Point(600, 58);
            this.Buton_Ürün_Göster.Name = "Buton_Ürün_Göster";
            this.Buton_Ürün_Göster.Size = new System.Drawing.Size(139, 23);
            this.Buton_Ürün_Göster.TabIndex = 17;
            this.Buton_Ürün_Göster.Text = "Ürünleri Görüntüle";
            this.Buton_Ürün_Göster.UseVisualStyleBackColor = true;
            this.Buton_Ürün_Göster.Click += new System.EventHandler(this.Buton_Ürün_Göster_Click);
            // 
            // Label_Ürün_Menüsü
            // 
            this.Label_Ürün_Menüsü.AutoSize = true;
            this.Label_Ürün_Menüsü.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Label_Ürün_Menüsü.Location = new System.Drawing.Point(594, 13);
            this.Label_Ürün_Menüsü.Name = "Label_Ürün_Menüsü";
            this.Label_Ürün_Menüsü.Size = new System.Drawing.Size(155, 29);
            this.Label_Ürün_Menüsü.TabIndex = 16;
            this.Label_Ürün_Menüsü.Text = "Ürün Menüsü";
            // 
            // Resim_Admin
            // 
            this.Resim_Admin.Location = new System.Drawing.Point(13, 13);
            this.Resim_Admin.Name = "Resim_Admin";
            this.Resim_Admin.Size = new System.Drawing.Size(126, 154);
            this.Resim_Admin.TabIndex = 0;
            this.Resim_Admin.TabStop = false;
            // 
            // Label_Eposta
            // 
            this.Label_Eposta.AutoSize = true;
            this.Label_Eposta.Location = new System.Drawing.Point(167, 154);
            this.Label_Eposta.Name = "Label_Eposta";
            this.Label_Eposta.Size = new System.Drawing.Size(40, 13);
            this.Label_Eposta.TabIndex = 27;
            this.Label_Eposta.Text = "Eposta";
            // 
            // Label_Admin_Eposta
            // 
            this.Label_Admin_Eposta.AutoSize = true;
            this.Label_Admin_Eposta.Location = new System.Drawing.Point(245, 154);
            this.Label_Admin_Eposta.Name = "Label_Admin_Eposta";
            this.Label_Admin_Eposta.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_Eposta.TabIndex = 28;
            // 
            // Label_Adres
            // 
            this.Label_Adres.AutoSize = true;
            this.Label_Adres.Location = new System.Drawing.Point(167, 40);
            this.Label_Adres.Name = "Label_Adres";
            this.Label_Adres.Size = new System.Drawing.Size(37, 13);
            this.Label_Adres.TabIndex = 2;
            this.Label_Adres.Text = "Soyad";
            // 
            // Label_Admin_Adres
            // 
            this.Label_Admin_Adres.AutoSize = true;
            this.Label_Admin_Adres.Location = new System.Drawing.Point(245, 40);
            this.Label_Admin_Adres.Name = "Label_Admin_Adres";
            this.Label_Admin_Adres.Size = new System.Drawing.Size(0, 13);
            this.Label_Admin_Adres.TabIndex = 7;
            // 
            // Buton_Müzik_Ekle
            // 
            this.Buton_Müzik_Ekle.Location = new System.Drawing.Point(600, 144);
            this.Buton_Müzik_Ekle.Name = "Buton_Müzik_Ekle";
            this.Buton_Müzik_Ekle.Size = new System.Drawing.Size(139, 23);
            this.Buton_Müzik_Ekle.TabIndex = 29;
            this.Buton_Müzik_Ekle.Text = "MüzikCD\'si Ekle";
            this.Buton_Müzik_Ekle.UseVisualStyleBackColor = true;
            this.Buton_Müzik_Ekle.Click += new System.EventHandler(this.Buton_Müzik_Ekle_Click);
            // 
            // Buton_Dergi_Ekle
            // 
            this.Buton_Dergi_Ekle.Location = new System.Drawing.Point(600, 115);
            this.Buton_Dergi_Ekle.Name = "Buton_Dergi_Ekle";
            this.Buton_Dergi_Ekle.Size = new System.Drawing.Size(139, 23);
            this.Buton_Dergi_Ekle.TabIndex = 30;
            this.Buton_Dergi_Ekle.Text = "Dergi Ekle";
            this.Buton_Dergi_Ekle.UseVisualStyleBackColor = true;
            this.Buton_Dergi_Ekle.Click += new System.EventHandler(this.Buton_Dergi_Ekle_Click);
            // 
            // Admin_Formu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 201);
            this.Controls.Add(this.Buton_Dergi_Ekle);
            this.Controls.Add(this.Buton_Müzik_Ekle);
            this.Controls.Add(this.Label_Admin_Eposta);
            this.Controls.Add(this.Label_Eposta);
            this.Controls.Add(this.Buton_Kitap_Ekle);
            this.Controls.Add(this.Buton_Ürün_Göster);
            this.Controls.Add(this.Label_Ürün_Menüsü);
            this.Controls.Add(this.Buton_Müşteri_Göster);
            this.Controls.Add(this.Label_Müşteri_Menü);
            this.Controls.Add(this.Buton_Müşteri_Onay);
            this.Controls.Add(this.Label_Admin_Nick);
            this.Controls.Add(this.Label_Admin_Rütbe);
            this.Controls.Add(this.Label_Admin_SonGiriş);
            this.Controls.Add(this.Label_Admin_Adres);
            this.Controls.Add(this.Label_Admin_İsim);
            this.Controls.Add(this.Label_Nick);
            this.Controls.Add(this.Label_Rütbe);
            this.Controls.Add(this.Label_SonGiriş);
            this.Controls.Add(this.Label_Adres);
            this.Controls.Add(this.Label_İsim);
            this.Controls.Add(this.Resim_Admin);
            this.Name = "Admin_Formu";
            this.Text = "Admin_Formu";
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Admin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Resim_Admin;
        private System.Windows.Forms.Label Label_İsim;
        private System.Windows.Forms.Label Label_SonGiriş;
        private System.Windows.Forms.Label Label_Rütbe;
        private System.Windows.Forms.Label Label_Nick;
        private System.Windows.Forms.Label Label_Admin_Nick;
        private System.Windows.Forms.Label Label_Admin_Rütbe;
        private System.Windows.Forms.Label Label_Admin_SonGiriş;
        private System.Windows.Forms.Label Label_Admin_İsim;
        private System.Windows.Forms.Button Buton_Müşteri_Onay;
        private System.Windows.Forms.Label Label_Müşteri_Menü;
        private System.Windows.Forms.Button Buton_Müşteri_Göster;
        private System.Windows.Forms.Button Buton_Kitap_Ekle;
        private System.Windows.Forms.Button Buton_Ürün_Göster;
        private System.Windows.Forms.Label Label_Ürün_Menüsü;
        private System.Windows.Forms.Label Label_Eposta;
        private System.Windows.Forms.Label Label_Admin_Eposta;
        private System.Windows.Forms.Label Label_Adres;
        private System.Windows.Forms.Label Label_Admin_Adres;
        private System.Windows.Forms.Button Buton_Müzik_Ekle;
        private System.Windows.Forms.Button Buton_Dergi_Ekle;
    }
}