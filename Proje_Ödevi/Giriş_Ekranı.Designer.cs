﻿namespace Proje_Ödevi
{
    partial class Giriş_Ekranı
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Text_TakmaAd = new System.Windows.Forms.TextBox();
            this.Text_Şifre = new System.Windows.Forms.TextBox();
            this.Label_TakmaAd = new System.Windows.Forms.Label();
            this.Label_Parola = new System.Windows.Forms.Label();
            this.Buton_Giriş = new System.Windows.Forms.Button();
            this.Buton_Kayıt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Text_TakmaAd
            // 
            this.Text_TakmaAd.Location = new System.Drawing.Point(125, 43);
            this.Text_TakmaAd.Name = "Text_TakmaAd";
            this.Text_TakmaAd.Size = new System.Drawing.Size(100, 20);
            this.Text_TakmaAd.TabIndex = 0;
            // 
            // Text_Şifre
            // 
            this.Text_Şifre.Location = new System.Drawing.Point(125, 80);
            this.Text_Şifre.Name = "Text_Şifre";
            this.Text_Şifre.Size = new System.Drawing.Size(100, 20);
            this.Text_Şifre.TabIndex = 1;
            // 
            // Label_TakmaAd
            // 
            this.Label_TakmaAd.AutoSize = true;
            this.Label_TakmaAd.Location = new System.Drawing.Point(49, 46);
            this.Label_TakmaAd.Name = "Label_TakmaAd";
            this.Label_TakmaAd.Size = new System.Drawing.Size(64, 13);
            this.Label_TakmaAd.TabIndex = 2;
            this.Label_TakmaAd.Text = "Kullanıcı Adı";
            // 
            // Label_Parola
            // 
            this.Label_Parola.AutoSize = true;
            this.Label_Parola.Location = new System.Drawing.Point(49, 83);
            this.Label_Parola.Name = "Label_Parola";
            this.Label_Parola.Size = new System.Drawing.Size(37, 13);
            this.Label_Parola.TabIndex = 3;
            this.Label_Parola.Tag = "*";
            this.Label_Parola.Text = "Parola";
            // 
            // Buton_Giriş
            // 
            this.Buton_Giriş.Location = new System.Drawing.Point(150, 116);
            this.Buton_Giriş.Name = "Buton_Giriş";
            this.Buton_Giriş.Size = new System.Drawing.Size(75, 23);
            this.Buton_Giriş.TabIndex = 4;
            this.Buton_Giriş.Text = "Giriş";
            this.Buton_Giriş.UseVisualStyleBackColor = true;
            this.Buton_Giriş.Click += new System.EventHandler(this.Buton_Giriş_Click);
            // 
            // Buton_Kayıt
            // 
            this.Buton_Kayıt.Location = new System.Drawing.Point(69, 116);
            this.Buton_Kayıt.Name = "Buton_Kayıt";
            this.Buton_Kayıt.Size = new System.Drawing.Size(75, 23);
            this.Buton_Kayıt.TabIndex = 5;
            this.Buton_Kayıt.Text = "Kayıt Ol";
            this.Buton_Kayıt.UseVisualStyleBackColor = true;
            this.Buton_Kayıt.Click += new System.EventHandler(this.Buton_Kayıt_Click);
            // 
            // Giriş_Ekranı
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 170);
            this.Controls.Add(this.Buton_Kayıt);
            this.Controls.Add(this.Buton_Giriş);
            this.Controls.Add(this.Label_Parola);
            this.Controls.Add(this.Label_TakmaAd);
            this.Controls.Add(this.Text_Şifre);
            this.Controls.Add(this.Text_TakmaAd);
            this.Name = "Giriş_Ekranı";
            this.Text = "Giriş_Ekranı";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Text_TakmaAd;
        private System.Windows.Forms.TextBox Text_Şifre;
        private System.Windows.Forms.Label Label_TakmaAd;
        private System.Windows.Forms.Label Label_Parola;
        private System.Windows.Forms.Button Buton_Giriş;
        private System.Windows.Forms.Button Buton_Kayıt;
    }
}