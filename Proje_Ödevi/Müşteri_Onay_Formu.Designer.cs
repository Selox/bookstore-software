﻿namespace Proje_Ödevi
{
    partial class Müşteri_Onay_Formu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Data_Müşteriler = new System.Windows.Forms.DataGridView();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buton_Onayla = new System.Windows.Forms.Button();
            this.Menü_Müşteri_Onay = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Customer_Confirmation = new System.Windows.Forms.ToolStripMenuItem();
            this.Customer_Rejection = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.Data_Müşteriler)).BeginInit();
            this.Menü_Müşteri_Onay.SuspendLayout();
            this.SuspendLayout();
            // 
            // Data_Müşteriler
            // 
            this.Data_Müşteriler.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Data_Müşteriler.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Name,
            this.Adress,
            this.Username,
            this.Email});
            this.Data_Müşteriler.Location = new System.Drawing.Point(13, 13);
            this.Data_Müşteriler.Name = "Data_Müşteriler";
            this.Data_Müşteriler.Size = new System.Drawing.Size(433, 232);
            this.Data_Müşteriler.TabIndex = 0;
            this.Data_Müşteriler.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Data_Müşteriler_CellMouseUp);
            // 
            // Name
            // 
            this.Name.HeaderText = "İsim";
            this.Name.Name = "Name";
            this.Name.Width = 80;
            // 
            // Adress
            // 
            this.Adress.HeaderText = "Adres";
            this.Adress.Name = "Adress";
            // 
            // Username
            // 
            this.Username.HeaderText = "Kullanıcı Adı";
            this.Username.Name = "Username";
            this.Username.Width = 70;
            // 
            // Email
            // 
            this.Email.HeaderText = "Eposta";
            this.Email.Name = "Email";
            this.Email.Width = 140;
            // 
            // Buton_Onayla
            // 
            this.Buton_Onayla.Location = new System.Drawing.Point(452, 13);
            this.Buton_Onayla.Name = "Buton_Onayla";
            this.Buton_Onayla.Size = new System.Drawing.Size(111, 37);
            this.Buton_Onayla.TabIndex = 1;
            this.Buton_Onayla.Text = "Kaydet ve Çık";
            this.Buton_Onayla.UseVisualStyleBackColor = true;
            this.Buton_Onayla.Click += new System.EventHandler(this.Buton_KaydetveÇık_Click);
            // 
            // Menü_Müşteri_Onay
            // 
            this.Menü_Müşteri_Onay.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Customer_Confirmation,
            this.Customer_Rejection});
            this.Menü_Müşteri_Onay.Name = "Menü_Müşteri_Onay";
            this.Menü_Müşteri_Onay.Size = new System.Drawing.Size(135, 48);
            // 
            // Customer_Confirmation
            // 
            this.Customer_Confirmation.Name = "Customer_Confirmation";
            this.Customer_Confirmation.Size = new System.Drawing.Size(134, 22);
            this.Customer_Confirmation.Text = "Üye Onayla";
            // 
            // Customer_Rejection
            // 
            this.Customer_Rejection.Name = "Customer_Rejection";
            this.Customer_Rejection.Size = new System.Drawing.Size(134, 22);
            this.Customer_Rejection.Text = "Sil";
            // 
            // Müşteri_Onay_Formu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 268);
            this.Controls.Add(this.Buton_Onayla);
            this.Controls.Add(this.Data_Müşteriler);
            this.Text = "Müşteri_Onay_Formu";
            ((System.ComponentModel.ISupportInitialize)(this.Data_Müşteriler)).EndInit();
            this.Menü_Müşteri_Onay.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Data_Müşteriler;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.Button Buton_Onayla;
        private System.Windows.Forms.ContextMenuStrip Menü_Müşteri_Onay;
        private System.Windows.Forms.ToolStripMenuItem Customer_Confirmation;
        private System.Windows.Forms.ToolStripMenuItem Customer_Rejection;
    }
}