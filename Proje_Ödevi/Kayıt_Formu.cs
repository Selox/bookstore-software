﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Proje_Ödevi
{
    public partial class Kayıt_Formu : Form
    {
        string PictureFile;

        public Kayıt_Formu()
        {
            InitializeComponent();
        }


        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        private void Buton_Kayıt_Tamamla_Click(object sender, EventArgs e)
        {
            if ((Text_Nick_Kayıt.Text != "" && Text_İsim_Kayıt.Text != "") && (Text_Şifre_Kayıt.Text.ToString() != "" && Text_Eposta_Kayıt.Text != ""))
            {
                if (Text_Şifre_Kayıt.Text.ToString() == Text_Şifre_Kayıt2.Text.ToString())
                {
                    string Path = "";
                    Path = @".\NewCustomers.txt";
                    string okuma = "";
                    if (File.Exists(Path))
                    {
                        StreamReader streamReader = new StreamReader(Path);
                        okuma += streamReader.ReadToEnd();
                        streamReader.Close();
                        int ide = RandomNumber(1, 1000);
                        okuma += ide.ToString() + "," + Text_İsim_Kayıt.Text + "," + Text_Adres_Kayıt.Text + "," + Text_Nick_Kayıt.Text + "," + Text_Eposta_Kayıt.Text + "," + Text_Şifre_Kayıt.Text.ToString() + "," + PictureFile + ","+ "false" + "," + "\n";
                        File.WriteAllText(Path, okuma);
                    }
                }
                else if ((Text_Şifre_Kayıt.Text.ToString() == Text_Şifre_Kayıt2.Text.ToString()) && (Text_Soyisim_Kayıt.Text != ""))
                {
                    string Path = "";
                    Path = @".\NewCustomers.txt";
                    string okuma = "";
                    if (File.Exists(Path))
                    {
                        StreamReader streamReader = new StreamReader(Path);
                        okuma += streamReader.ReadToEnd();
                        streamReader.Close();
                        int ide = RandomNumber(1, 1000);
                        okuma += ide.ToString() + "," + Text_İsim_Kayıt.Text + "," + Text_Adres_Kayıt.Text + "," + Text_Nick_Kayıt.Text + "," + Text_Eposta_Kayıt.Text + "," + Text_Şifre_Kayıt.Text.ToString() + "," + "false" + "," + "\n";
                        File.WriteAllText(Path, okuma);
                    }
                }
                else
                {
                    MessageBox.Show("Şifreleriniz Uyuşmamaktadır!", "Hatalı Giriş!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Text_Şifre_Kayıt.Clear();
                    Text_Şifre_Kayıt2.Clear();
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Lütfen Gerekli Alanları Doldurunuz!", "Hatalı Giriş!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text_İsim_Kayıt.Clear();
                Text_Soyisim_Kayıt.Clear();
                Text_Adres_Kayıt.Clear();
                Text_Nick_Kayıt.Clear();
                Text_Şifre_Kayıt.Clear();
                Text_Şifre_Kayıt2.Clear();
                Text_Eposta_Kayıt.Clear();
            }
            
        }

        private void Buton_Resim_Yükle_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Dosya Açma";
            open.Filter = "Image Files(*.jpg; *.jpeg; *.bmp)|*.jpg; *.jpeg; *.bmp";
            open.InitialDirectory = @"d:\";
            if (open.ShowDialog() == DialogResult.OK)
            {
                Text_Resim.Text = open.FileName;
                Bitmap Resim = (Bitmap)Image.FromFile(Text_Resim.Text);
                Bitmap resized = new Bitmap(Resim, new Size(120, 150));
                resized.Save("NewCustomer");
                Resim_Kayıt.Image = Resim;
                PictureFile = Text_Resim.Text;
            }

        }
    }
}
