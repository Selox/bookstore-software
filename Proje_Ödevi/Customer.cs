﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Proje_Ödevi
{
    /** 
     * \brief Customer sınıfı.
     *  Sisteme giriş yapacak müşterilerin oluşturulmasını ve müşteri işlemlerinin gerçekleşmesini sağlayan sınıftır.
     *  @param CustomerID kişi id'sini tutan parametre
     *  @param Name kişi isminin tutan parametre
     *  @param Adress kişi adresini tutan parametre
     *  @param Username kişi adresini tutan parametre
     *  @param Email kişi mailini tutan parametre
     *  @param Password kişi şifresini tutan parametre
     *  @param Exist kişinin hesabının onaylanıp onaylanmadığını tutan tutan parametre.
     *  @param Customer_List müşterilerin oluşturulduğu ve saklandığı liste.
   */
    public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public int Password { get; set; }
        public string Picture { get; set; }
        public bool Exist { get; set; }
        public List<Customer> Customer_List = new List<Customer>();
        /**
     * Müşteri bilgilerini yazdırmak için kullanılan metod.
     * @see printCustomerDetails(int id)
     * @param id bilgilerine bakılacak kişinin liste indeksini tutan parametre.
     */
        public string printCustomerDetails(int id)
        {
            int i = 0, number = 0;
            for (; i < Customer_List.Count; i++)
            {
                if (id == Customer_List[i].CustomerID)
                {
                    number = i;
                    break;
                }
            }
            string informations = "";
            informations += Customer_List[number].Name;
            informations += Customer_List[number].Adress;
            informations += Customer_List[number].Username;
            informations += Customer_List[number].Email;
            informations += Customer_List[number].Picture;
            return informations;

        }
        /**
      * Müşteri bilgisini döndüren metod.
      * @see SaveCustomer(Customer customer)
      */
        public Customer SaveCustomer(Customer customer)
        {
            return customer;
        }
        /**
     * Müşterinin satın alma geçmişini yazdıran metod.
     * @see printCustomerPurchases(Customer customer)
     * @param customer müşteriyi tutan parametre.
     */
        public void printCustomerPurchases(Customer customer)
        {
            //if (File.Exists(customer.CustomerID.ToString() + ".txt"))
            //{


            //}
            //else
            //{
            //    using (FileStream fs = File.Create(customer.CustomerID.ToString() + ".txt"))
            //    {
            //        // Add some text to file    
            //        Byte[] title = new UTF8Encoding(true).GetBytes("New Text File");
            //        fs.Write(title, 0, title.Length);
            //        byte[] author = new UTF8Encoding(true).GetBytes("Mahesh Chand");
            //        fs.Write(author, 0, author.Length);
            //    }
            //}

        }
        /**
      * Müşteri listesini döndüren metod.
      * @see LoadUp()
      */
        public List<Customer> LoadUp()
        {
            return Customer_List;
        }
        /**
      * Müşteri listesine müşteri ekleyen metod.
      * @see addCustomer(int id, string name, string  adress, string username,string  email,int password,bool system)
      */
        public void addCustomer(int id,string name,string adress,string username,string email,int password,bool system)
        {
            Customer_List.Add(new Customer
            {
                CustomerID = id,
                Name = name,
                Adress = adress,
                Username = username,
                Email = email,
                Password = password,
                Exist = system
            });
        }
        /**
      * Mğşteri bilgilerini yazdırmak için kullanılan ön metod.
      * @see listThemAll(string x)
      * @param x tüm bilgilerin tutulduğu stringdir.
      */
        public string listThemAll(string x)
        {
            string res = "";
            for (int i = 0; i < Customer_List.Count; i++)
            {
                res += Customer_List[i].CustomerID.ToString() + x;
                res += Customer_List[i].Name + x;
                res += Customer_List[i].Adress + x;
                res += Customer_List[i].Username + x;
                res += Customer_List[i].Email + x;
                res += Customer_List[i].Password + x;
                res += Customer_List[i].Exist + x;
                res += Customer_List[i].Picture + x;
                res += Environment.NewLine;
            }
            return res;
        }


    }
}
