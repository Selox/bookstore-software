﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Proje_Ödevi
{
    /** 
    * \brief Alışveriş Sepeti formu.
    *  Müşterilerin alışveriş yapabilmeleri içib shoppingcart sınıfı ile beraber çalışan arayüz formudur.
  */
    public partial class AlışverişSepeti : Form
    {

        int CustomerID;
        Customer customer;
        List<ItemtoPurchase> items = new List<ItemtoPurchase>();
        ItemtoPurchase item = new ItemtoPurchase();
        ShoppingCart sepet = new ShoppingCart();
        double paymentAmount;
        string paymentType;
        List<Book> kitaplar = new List<Book>();
        List<Magazine> dergiler = new List<Magazine>();
        List<MusicCD> müzikler = new List<MusicCD>();
        string[] products;
        int rowIndex;
        int flag = 0;
        public AlışverişSepeti(int id, Customer cust)
        {
            customer = cust;
            CustomerID = id;
            InitializeComponent();
            Yükle_Ürünler();
            
        }

        private void Yükle_Ürünler()
        {
            string Path = "";
            Path = @".\product.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        if (elements[0] == "Book")
                        {
                            Book Bo = new Book();
                            Bo.id = Int32.Parse(elements[1]);
                            Bo.name = elements[2];
                            Bo.price = Double.Parse(elements[3]);
                            Bo.ISBN = Int32.Parse(elements[4]);
                            Bo.author = elements[5];
                            Bo.publisher = elements[6];
                            Bo.page= Int32.Parse(elements[7]);
                            Bo.cover_page_picture = @elements[8];
                            string[] row = new string[] { elements[1], elements[2], elements[3], elements[4], elements[5], elements[6], elements[7] };
                            Book_DataGrid.Rows.Add(row);
                            //kitaplar.Add(new Book { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]), ISBN = Int32.Parse(elements[4]), author = elements[5],publisher = elements[6],page =  Int32.Parse(elements[7]) });
                            kitaplar.Add(Bo);
                        }
                        else if (elements[0] == "Magazine")
                        {
                            Magazine Mag = new Magazine();
                            Mag.id = Int32.Parse(elements[1]);
                            Mag.name = elements[2];
                            Mag.price = Double.Parse(elements[3]);
                            Mag.issue = Int32.Parse(elements[4]);
                            Mag.type = elements[5];
                            Mag.picture = @elements[6];
                            string[] row = new string[] { elements[1], elements[2], elements[3], elements[4], elements[5] };
                            Magazine_DataGrid.Rows.Add(row);
                            //dergiler.Add(new Magazine { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]), issue = Int32.Parse(elements[4]), type = elements[5]});
                            dergiler.Add(Mag);
                        }
                        else
                        {
                            MusicCD CD = new MusicCD();
                            CD.id = Int32.Parse(elements[1]);
                            CD.name = elements[2];
                            CD.price = Double.Parse(elements[3]);
                            CD.singer = elements[4];
                            CD.type = elements[5];
                            CD.picture = @elements[6];
                            string[] row = new string[] { elements[1], elements[2], elements[3], elements[4], elements[5] };
                            Music_DataGrid.Rows.Add(row);
                            //müzikler.Add(new MusicCD { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]),  singer = elements[4], type = elements[5] });
                            müzikler.Add(CD);
                        }
                    }

                }
            }
        }

        private void Book_DataGrid_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Book_DataGrid.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Book_DataGrid.CurrentCell = this.Book_DataGrid.Rows[e.RowIndex].Cells[1];
                this.Menü_Alışveriş_Sepeti.Show(this.Book_DataGrid, e.Location);
                Menü_Alışveriş_Sepeti.Show(Cursor.Position);
                flag = 1;
            }
        }

        private void Magazine_DataGrid_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Magazine_DataGrid.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Magazine_DataGrid.CurrentCell = this.Magazine_DataGrid.Rows[e.RowIndex].Cells[1];
                this.Menü_Alışveriş_Sepeti.Show(this.Magazine_DataGrid, e.Location);
                Menü_Alışveriş_Sepeti.Show(Cursor.Position);
                flag = 2;
            }
        }

        private void Music_DataGrid_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Music_DataGrid.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Music_DataGrid.CurrentCell = this.Music_DataGrid.Rows[e.RowIndex].Cells[1];
                this.Menü_Alışveriş_Sepeti.Show(this.Music_DataGrid, e.Location);
                Menü_Alışveriş_Sepeti.Show(Cursor.Position);
                flag = 3;
            }
        }

        private void Add_to_Basket_Click(object sender, EventArgs e)
        {
            bool onay = false;
            int i = 0;
            if(flag == 1)
            {
                if (items.Capacity != 0)
                {
                    for (i = 0; i < items.Count; i++)
                    {
                        if (items[i].id.ToString() == kitaplar[rowIndex].id.ToString())
                        {
                            onay = true;
                            break;
                        }
                    }
                    if (onay == true)
                    {
                        int miktar;
                        items[i].quantity++;
                        miktar = int.Parse(Data_Ürünler.Rows[i].Cells["Quantity"].Value.ToString());
                        miktar++;
                        Data_Ürünler.Rows[i].Cells["Quantity"].Value = miktar.ToString();
                    }
                    else
                    {
                        string[] row = new string[] { kitaplar[rowIndex].id.ToString(), kitaplar[rowIndex].name, kitaplar[rowIndex].price.ToString(), 1.ToString() };
                        Data_Ürünler.Rows.Add(row);
                        //items.Add(new ItemtoPurchase { id = kitaplar[rowIndex].id, name = kitaplar[rowIndex].name, price = kitaplar[rowIndex].price, quantity = 1 });
                        sepet.addProduct(new ItemtoPurchase { id = kitaplar[rowIndex].id, name = kitaplar[rowIndex].name, price = kitaplar[rowIndex].price, quantity = 1 });
                    }
                    
                }
                else
                {
                    string[] row = new string[] { kitaplar[rowIndex].id.ToString(), kitaplar[rowIndex].name, kitaplar[rowIndex].price.ToString(), 1.ToString() };
                    Data_Ürünler.Rows.Add(row);
                    //items.Add(new ItemtoPurchase { id = kitaplar[rowIndex].id, name = kitaplar[rowIndex].name,price = kitaplar[rowIndex].price,quantity = 1 });
                    sepet.addProduct(new ItemtoPurchase { id = kitaplar[rowIndex].id, name = kitaplar[rowIndex].name, price = kitaplar[rowIndex].price, quantity = 1 });
                }
                flag = 0;
                Book_DataGrid.ClearSelection();
                rowIndex = 0;
                onay = false;
            }
            else if(flag == 2)
            {
                if(items.Capacity != 0)
                {
                    for (i = 0; i < items.Count; i++)
                    {
                        if (items[i].id.ToString() == dergiler[rowIndex].id.ToString())
                        {
                            onay = true;
                            break;
                        }
                    }
                    if (onay == true)
                    {
                        int miktar;
                        items[i].quantity++;
                        miktar = int.Parse(Data_Ürünler.Rows[i].Cells["Quantity"].Value.ToString());
                        miktar++;
                        Data_Ürünler.Rows[i].Cells["Quantity"].Value = miktar.ToString();
                    }
                    else
                    {
                        string[] row = new string[] { dergiler[rowIndex].id.ToString(), dergiler[rowIndex].name, dergiler[rowIndex].price.ToString(), 1.ToString() };
                        Data_Ürünler.Rows.Add(row);
                        sepet.addProduct(new ItemtoPurchase { id = dergiler[rowIndex].id, name = dergiler[rowIndex].name, price = dergiler[rowIndex].price, quantity = 1 });
                        //items.Add(new ItemtoPurchase { id = dergiler[rowIndex].id, name = dergiler[rowIndex].name, price = dergiler[rowIndex].price, quantity = 1 });
                    }
                }
                else
                {
                    string[] row = new string[] { dergiler[rowIndex].id.ToString(), dergiler[rowIndex].name, dergiler[rowIndex].price.ToString(), 1.ToString() };
                    Data_Ürünler.Rows.Add(row);
                    sepet.addProduct(new ItemtoPurchase { id = dergiler[rowIndex].id, name = dergiler[rowIndex].name, price = dergiler[rowIndex].price, quantity = 1 });
                    //items.Add(new ItemtoPurchase { id = dergiler[rowIndex].id, name = dergiler[rowIndex].name, price = dergiler[rowIndex].price, quantity = 1 });
                }
                flag = 0;
                Magazine_DataGrid.ClearSelection();
                rowIndex = 0;
                onay = false;
            }
            else if(flag == 3)
            {
                if (items.Capacity != 0)
                {
                    for (i = 0; i < items.Count; i++)
                    {
                        if (items[i].id.ToString() == müzikler[rowIndex].id.ToString())
                        {
                            onay = true;
                            break;
                        }
                    }
                    if (onay == true)
                    {
                        int miktar;
                        items[i].quantity++;
                        miktar = int.Parse(Data_Ürünler.Rows[i].Cells["Quantity"].Value.ToString());
                        miktar++;
                        Data_Ürünler.Rows[i].Cells["Quantity"].Value = miktar.ToString();
                    }
                    else
                    {
                        string[] row = new string[] { müzikler[rowIndex].id.ToString(), müzikler[rowIndex].name, müzikler[rowIndex].price.ToString(), 1.ToString() };
                        Data_Ürünler.Rows.Add(row);
                        sepet.addProduct(new ItemtoPurchase { id = müzikler[rowIndex].id, name = müzikler[rowIndex].name, price = müzikler[rowIndex].price, quantity = 1 });
                        //items.Add(new ItemtoPurchase { id = müzikler[rowIndex].id, name = müzikler[rowIndex].name, price = müzikler[rowIndex].price, quantity = 1 });
                    }
                }
                else
                {
                    string[] row = new string[] { müzikler[rowIndex].id.ToString(), müzikler[rowIndex].name, müzikler[rowIndex].price.ToString(), 1.ToString() };
                    Data_Ürünler.Rows.Add(row);
                    sepet.addProduct(new ItemtoPurchase { id = müzikler[rowIndex].id, name = müzikler[rowIndex].name, price = müzikler[rowIndex].price, quantity = 1 }); 
                    //items.Add(new ItemtoPurchase { id = müzikler[rowIndex].id, name = müzikler[rowIndex].name, price = müzikler[rowIndex].price, quantity = 1 });
                }
                flag = 0;
                Magazine_DataGrid.ClearSelection();
                rowIndex = 0;
                onay = false;
            }
            else
            {
                flag = 0;
            }
            Data_Ürünler.ClearSelection();
        }

        private void Remove_From_Basket_Click(object sender, EventArgs e)
        {
            items = sepet.LoadUp();
            if (flag == 4)
            {
                if (!this.Data_Ürünler.Rows[this.rowIndex].IsNewRow)
                {
                    this.Data_Ürünler.Rows.RemoveAt(this.rowIndex);
                    this.items.RemoveAt(this.rowIndex);
                    Data_Ürünler.ClearSelection();
                    sepet.removeProduct(items[rowIndex]);
                    rowIndex = 0;
                }
                flag = 0;
            }
            else
            {
                flag = 0;
            }
        }

        private void Clear_Selected_Click(object sender, EventArgs e)
        {
            string olay = "Seçilen ürünleri temizlendi";
            customer.logAdd(olay);
            Book_DataGrid.ClearSelection();
            Magazine_DataGrid.ClearSelection();
            Music_DataGrid.ClearSelection();
            Data_Ürünler.ClearSelection();
        }

        private void Data_Ürünler_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Data_Ürünler.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Data_Ürünler.CurrentCell = this.Data_Ürünler.Rows[e.RowIndex].Cells[1];
                this.Menü_Alışveriş_Sepeti.Show(this.Data_Ürünler, e.Location);
                Menü_Alışveriş_Sepeti.Show(Cursor.Position);
                flag = 4;  
            }
        }

        private void Book_DataGrid_SelectionChanged(object sender, EventArgs e)
        {
            int index;
            index = this.Book_DataGrid.CurrentCell.RowIndex;
            Resim_Ürün.ImageLocation = kitaplar[index].cover_page_picture;
            Resim_Ürün.SizeMode = PictureBoxSizeMode.StretchImage;
            Book_DataGrid.ClearSelection();
        }

        private void Magazine_DataGrid_SelectionChanged(object sender, EventArgs e)
        {
            int index;
            index = this.Magazine_DataGrid.CurrentCell.RowIndex;
            Resim_Ürün.ImageLocation = dergiler[index].picture;
            Resim_Ürün.SizeMode = PictureBoxSizeMode.StretchImage;
            Magazine_DataGrid.ClearSelection();
        }

        private void Music_DataGrid_SelectionChanged(object sender, EventArgs e)
        {
            int index;
            index = this.Music_DataGrid.CurrentCell.RowIndex;
            Resim_Ürün.ImageLocation = müzikler[index].picture;
            Resim_Ürün.SizeMode = PictureBoxSizeMode.StretchImage;
            Music_DataGrid.ClearSelection();
        }

        private void Buton_İptal_Et_Click(object sender, EventArgs e)
        {
            string olay = "Alışverişi İptal Et butonı Tıklandı";
            customer.logAdd(olay);
            sepet.cancelOrder(customer);
        }

        private void Buton_Alışveriş_Tamamla_Click(object sender, EventArgs e)
        {
            string olay = "Alışverişi Tamamla Buton Tık";
            customer.logAdd(olay);
            paymentAmount = sepet.placeOrder(customer);
            this.Close();
        }
    }
}
