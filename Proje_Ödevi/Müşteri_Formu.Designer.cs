﻿namespace Proje_Ödevi
{
    partial class Müşteri_Formu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Müşteri_Eposta = new System.Windows.Forms.Label();
            this.Label_Eposta = new System.Windows.Forms.Label();
            this.Label_Müşteri_Nick = new System.Windows.Forms.Label();
            this.Label_Müşteri_SonGiriş = new System.Windows.Forms.Label();
            this.Label_Müşteri_İsim = new System.Windows.Forms.Label();
            this.Label_Nick = new System.Windows.Forms.Label();
            this.Label_SonGiriş = new System.Windows.Forms.Label();
            this.Label_İsim = new System.Windows.Forms.Label();
            this.Resim_Müşteri = new System.Windows.Forms.PictureBox();
            this.Buton_Alışveriş = new System.Windows.Forms.Button();
            this.Buton_Kaydet_Çık = new System.Windows.Forms.Button();
            this.Label_Adres = new System.Windows.Forms.Label();
            this.Label_Müşteri_Adres = new System.Windows.Forms.Label();
            this.Buton_Bilgileri_Güncelle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Müşteri)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_Müşteri_Eposta
            // 
            this.Label_Müşteri_Eposta.AutoSize = true;
            this.Label_Müşteri_Eposta.Location = new System.Drawing.Point(244, 127);
            this.Label_Müşteri_Eposta.Name = "Label_Müşteri_Eposta";
            this.Label_Müşteri_Eposta.Size = new System.Drawing.Size(0, 13);
            this.Label_Müşteri_Eposta.TabIndex = 41;
            // 
            // Label_Eposta
            // 
            this.Label_Eposta.AutoSize = true;
            this.Label_Eposta.Location = new System.Drawing.Point(166, 127);
            this.Label_Eposta.Name = "Label_Eposta";
            this.Label_Eposta.Size = new System.Drawing.Size(40, 13);
            this.Label_Eposta.TabIndex = 40;
            this.Label_Eposta.Text = "Eposta";
            // 
            // Label_Müşteri_Nick
            // 
            this.Label_Müşteri_Nick.AutoSize = true;
            this.Label_Müşteri_Nick.Location = new System.Drawing.Point(244, 70);
            this.Label_Müşteri_Nick.Name = "Label_Müşteri_Nick";
            this.Label_Müşteri_Nick.Size = new System.Drawing.Size(0, 13);
            this.Label_Müşteri_Nick.TabIndex = 39;
            // 
            // Label_Müşteri_SonGiriş
            // 
            this.Label_Müşteri_SonGiriş.AutoSize = true;
            this.Label_Müşteri_SonGiriş.Location = new System.Drawing.Point(244, 99);
            this.Label_Müşteri_SonGiriş.Name = "Label_Müşteri_SonGiriş";
            this.Label_Müşteri_SonGiriş.Size = new System.Drawing.Size(0, 13);
            this.Label_Müşteri_SonGiriş.TabIndex = 37;
            // 
            // Label_Müşteri_İsim
            // 
            this.Label_Müşteri_İsim.AutoSize = true;
            this.Label_Müşteri_İsim.Location = new System.Drawing.Point(244, 12);
            this.Label_Müşteri_İsim.Name = "Label_Müşteri_İsim";
            this.Label_Müşteri_İsim.Size = new System.Drawing.Size(0, 13);
            this.Label_Müşteri_İsim.TabIndex = 35;
            // 
            // Label_Nick
            // 
            this.Label_Nick.AutoSize = true;
            this.Label_Nick.Location = new System.Drawing.Point(166, 70);
            this.Label_Nick.Name = "Label_Nick";
            this.Label_Nick.Size = new System.Drawing.Size(29, 13);
            this.Label_Nick.TabIndex = 34;
            this.Label_Nick.Text = "Nick";
            // 
            // Label_SonGiriş
            // 
            this.Label_SonGiriş.AutoSize = true;
            this.Label_SonGiriş.Location = new System.Drawing.Point(166, 99);
            this.Label_SonGiriş.Name = "Label_SonGiriş";
            this.Label_SonGiriş.Size = new System.Drawing.Size(49, 13);
            this.Label_SonGiriş.TabIndex = 32;
            this.Label_SonGiriş.Text = "Son Giriş";
            // 
            // Label_İsim
            // 
            this.Label_İsim.AutoSize = true;
            this.Label_İsim.Location = new System.Drawing.Point(166, 12);
            this.Label_İsim.Name = "Label_İsim";
            this.Label_İsim.Size = new System.Drawing.Size(20, 13);
            this.Label_İsim.TabIndex = 30;
            this.Label_İsim.Text = "Ad";
            // 
            // Resim_Müşteri
            // 
            this.Resim_Müşteri.Location = new System.Drawing.Point(12, 12);
            this.Resim_Müşteri.Name = "Resim_Müşteri";
            this.Resim_Müşteri.Size = new System.Drawing.Size(126, 154);
            this.Resim_Müşteri.TabIndex = 29;
            this.Resim_Müşteri.TabStop = false;
            // 
            // Buton_Alışveriş
            // 
            this.Buton_Alışveriş.Location = new System.Drawing.Point(384, 12);
            this.Buton_Alışveriş.Name = "Buton_Alışveriş";
            this.Buton_Alışveriş.Size = new System.Drawing.Size(75, 23);
            this.Buton_Alışveriş.TabIndex = 42;
            this.Buton_Alışveriş.Text = "Alışveriş Yap";
            this.Buton_Alışveriş.UseVisualStyleBackColor = true;
            this.Buton_Alışveriş.Click += new System.EventHandler(this.Buton_Alışveriş_Click);
            // 
            // Buton_Kaydet_Çık
            // 
            this.Buton_Kaydet_Çık.Location = new System.Drawing.Point(384, 83);
            this.Buton_Kaydet_Çık.Name = "Buton_Kaydet_Çık";
            this.Buton_Kaydet_Çık.Size = new System.Drawing.Size(75, 36);
            this.Buton_Kaydet_Çık.TabIndex = 43;
            this.Buton_Kaydet_Çık.Text = "Kaydet ve Çıkış Yap";
            this.Buton_Kaydet_Çık.UseVisualStyleBackColor = true;
            this.Buton_Kaydet_Çık.Click += new System.EventHandler(this.Buton_Kaydet_Çık_Click);
            // 
            // Label_Adres
            // 
            this.Label_Adres.AutoSize = true;
            this.Label_Adres.Location = new System.Drawing.Point(166, 40);
            this.Label_Adres.Name = "Label_Adres";
            this.Label_Adres.Size = new System.Drawing.Size(34, 13);
            this.Label_Adres.TabIndex = 31;
            this.Label_Adres.Text = "Adres";
            // 
            // Label_Müşteri_Adres
            // 
            this.Label_Müşteri_Adres.AutoSize = true;
            this.Label_Müşteri_Adres.Location = new System.Drawing.Point(244, 40);
            this.Label_Müşteri_Adres.Name = "Label_Müşteri_Adres";
            this.Label_Müşteri_Adres.Size = new System.Drawing.Size(0, 13);
            this.Label_Müşteri_Adres.TabIndex = 36;
            // 
            // Buton_Bilgileri_Güncelle
            // 
            this.Buton_Bilgileri_Güncelle.Location = new System.Drawing.Point(384, 41);
            this.Buton_Bilgileri_Güncelle.Name = "Buton_Bilgileri_Güncelle";
            this.Buton_Bilgileri_Güncelle.Size = new System.Drawing.Size(75, 36);
            this.Buton_Bilgileri_Güncelle.TabIndex = 44;
            this.Buton_Bilgileri_Güncelle.Text = "Bilgilerimi Güncelle";
            this.Buton_Bilgileri_Güncelle.UseVisualStyleBackColor = true;
            this.Buton_Bilgileri_Güncelle.Click += new System.EventHandler(this.Buton_Bilgileri_Güncelle_Click);
            // 
            // Müşteri_Formu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 215);
            this.Controls.Add(this.Buton_Bilgileri_Güncelle);
            this.Controls.Add(this.Buton_Kaydet_Çık);
            this.Controls.Add(this.Buton_Alışveriş);
            this.Controls.Add(this.Label_Müşteri_Eposta);
            this.Controls.Add(this.Label_Eposta);
            this.Controls.Add(this.Label_Müşteri_Nick);
            this.Controls.Add(this.Label_Müşteri_SonGiriş);
            this.Controls.Add(this.Label_Müşteri_Adres);
            this.Controls.Add(this.Label_Müşteri_İsim);
            this.Controls.Add(this.Label_Nick);
            this.Controls.Add(this.Label_SonGiriş);
            this.Controls.Add(this.Label_Adres);
            this.Controls.Add(this.Label_İsim);
            this.Controls.Add(this.Resim_Müşteri);
            this.Name = "Müşteri_Formu";
            this.Text = "Müşteri_Formu";
            this.Load += new System.EventHandler(this.Müşteri_Formu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Müşteri)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Müşteri_Eposta;
        private System.Windows.Forms.Label Label_Eposta;
        private System.Windows.Forms.Label Label_Müşteri_Nick;
        private System.Windows.Forms.Label Label_Müşteri_SonGiriş;
        private System.Windows.Forms.Label Label_Müşteri_İsim;
        private System.Windows.Forms.Label Label_Nick;
        private System.Windows.Forms.Label Label_SonGiriş;
        private System.Windows.Forms.Label Label_İsim;
        private System.Windows.Forms.PictureBox Resim_Müşteri;
        private System.Windows.Forms.Button Buton_Alışveriş;
        private System.Windows.Forms.Button Buton_Kaydet_Çık;
        private System.Windows.Forms.Label Label_Adres;
        private System.Windows.Forms.Label Label_Müşteri_Adres;
        private System.Windows.Forms.Button Buton_Bilgileri_Güncelle;
    }
}