﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje_Ödevi
{
    /** 
     * \brief Magazine sınıfı.
     *  Sistemdeki dergilerin oluşturulması için gereken sınıftır.
     *  @param issue cilt bilgisini tutan parametre
     *  @param type derginin türünü tutan parametre
     *  @param picture derginin kapak sayfasını tutan parametre
   */
    class Magazine : Product
    {
        public int issue { get; set; }
        public string type { get; set; }
        public string picture { get; set; }
    }
}
