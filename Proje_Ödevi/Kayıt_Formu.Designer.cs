﻿namespace Proje_Ödevi
{
    partial class Kayıt_Formu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Eposta_Kayıt = new System.Windows.Forms.Label();
            this.Label_Nick_Kayıt = new System.Windows.Forms.Label();
            this.Label_Soyisim_Kayıt = new System.Windows.Forms.Label();
            this.Label_İsim_Kayıt = new System.Windows.Forms.Label();
            this.Resim_Kayıt = new System.Windows.Forms.PictureBox();
            this.Text_İsim_Kayıt = new System.Windows.Forms.TextBox();
            this.Text_Soyisim_Kayıt = new System.Windows.Forms.TextBox();
            this.Text_Nick_Kayıt = new System.Windows.Forms.TextBox();
            this.Text_Eposta_Kayıt = new System.Windows.Forms.TextBox();
            this.Text_Şifre_Kayıt = new System.Windows.Forms.TextBox();
            this.Label_Şifre_Kayıt = new System.Windows.Forms.Label();
            this.Text_Şifre_Kayıt2 = new System.Windows.Forms.TextBox();
            this.Label_Şifre_Kayıt2 = new System.Windows.Forms.Label();
            this.Buton_Kayıt_Tamamla = new System.Windows.Forms.Button();
            this.Dosya_Resim_Aç = new System.Windows.Forms.OpenFileDialog();
            this.Buton_Resim_Yükle = new System.Windows.Forms.Button();
            this.Text_Adres_Kayıt = new System.Windows.Forms.TextBox();
            this.Label_Adres_Kayıt = new System.Windows.Forms.Label();
            this.Text_Resim = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Kayıt)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_Eposta_Kayıt
            // 
            this.Label_Eposta_Kayıt.AutoSize = true;
            this.Label_Eposta_Kayıt.Location = new System.Drawing.Point(157, 125);
            this.Label_Eposta_Kayıt.Name = "Label_Eposta_Kayıt";
            this.Label_Eposta_Kayıt.Size = new System.Drawing.Size(56, 13);
            this.Label_Eposta_Kayıt.TabIndex = 33;
            this.Label_Eposta_Kayıt.Text = "E-postanız";
            // 
            // Label_Nick_Kayıt
            // 
            this.Label_Nick_Kayıt.AutoSize = true;
            this.Label_Nick_Kayıt.Location = new System.Drawing.Point(157, 97);
            this.Label_Nick_Kayıt.Name = "Label_Nick_Kayıt";
            this.Label_Nick_Kayıt.Size = new System.Drawing.Size(80, 13);
            this.Label_Nick_Kayıt.TabIndex = 32;
            this.Label_Nick_Kayıt.Text = "Kullanıcı İsminiz";
            // 
            // Label_Soyisim_Kayıt
            // 
            this.Label_Soyisim_Kayıt.AutoSize = true;
            this.Label_Soyisim_Kayıt.Location = new System.Drawing.Point(157, 46);
            this.Label_Soyisim_Kayıt.Name = "Label_Soyisim_Kayıt";
            this.Label_Soyisim_Kayıt.Size = new System.Drawing.Size(52, 13);
            this.Label_Soyisim_Kayıt.TabIndex = 29;
            this.Label_Soyisim_Kayıt.Text = "Soyadınız";
            // 
            // Label_İsim_Kayıt
            // 
            this.Label_İsim_Kayıt.AutoSize = true;
            this.Label_İsim_Kayıt.Location = new System.Drawing.Point(158, 21);
            this.Label_İsim_Kayıt.Name = "Label_İsim_Kayıt";
            this.Label_İsim_Kayıt.Size = new System.Drawing.Size(35, 13);
            this.Label_İsim_Kayıt.TabIndex = 28;
            this.Label_İsim_Kayıt.Text = "Adınız";
            // 
            // Resim_Kayıt
            // 
            this.Resim_Kayıt.Location = new System.Drawing.Point(13, 13);
            this.Resim_Kayıt.Name = "Resim_Kayıt";
            this.Resim_Kayıt.Size = new System.Drawing.Size(120, 150);
            this.Resim_Kayıt.TabIndex = 34;
            this.Resim_Kayıt.TabStop = false;
            // 
            // Text_İsim_Kayıt
            // 
            this.Text_İsim_Kayıt.Location = new System.Drawing.Point(243, 16);
            this.Text_İsim_Kayıt.Name = "Text_İsim_Kayıt";
            this.Text_İsim_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_İsim_Kayıt.TabIndex = 35;
            // 
            // Text_Soyisim_Kayıt
            // 
            this.Text_Soyisim_Kayıt.Location = new System.Drawing.Point(243, 42);
            this.Text_Soyisim_Kayıt.Name = "Text_Soyisim_Kayıt";
            this.Text_Soyisim_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_Soyisim_Kayıt.TabIndex = 36;
            // 
            // Text_Nick_Kayıt
            // 
            this.Text_Nick_Kayıt.Location = new System.Drawing.Point(243, 94);
            this.Text_Nick_Kayıt.Name = "Text_Nick_Kayıt";
            this.Text_Nick_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_Nick_Kayıt.TabIndex = 37;
            // 
            // Text_Eposta_Kayıt
            // 
            this.Text_Eposta_Kayıt.Location = new System.Drawing.Point(243, 122);
            this.Text_Eposta_Kayıt.Name = "Text_Eposta_Kayıt";
            this.Text_Eposta_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_Eposta_Kayıt.TabIndex = 38;
            // 
            // Text_Şifre_Kayıt
            // 
            this.Text_Şifre_Kayıt.Location = new System.Drawing.Point(243, 148);
            this.Text_Şifre_Kayıt.Name = "Text_Şifre_Kayıt";
            this.Text_Şifre_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_Şifre_Kayıt.TabIndex = 40;
            // 
            // Label_Şifre_Kayıt
            // 
            this.Label_Şifre_Kayıt.AutoSize = true;
            this.Label_Şifre_Kayıt.Location = new System.Drawing.Point(157, 151);
            this.Label_Şifre_Kayıt.Name = "Label_Şifre_Kayıt";
            this.Label_Şifre_Kayıt.Size = new System.Drawing.Size(41, 13);
            this.Label_Şifre_Kayıt.TabIndex = 39;
            this.Label_Şifre_Kayıt.Text = "Şifreniz";
            // 
            // Text_Şifre_Kayıt2
            // 
            this.Text_Şifre_Kayıt2.Location = new System.Drawing.Point(243, 174);
            this.Text_Şifre_Kayıt2.Name = "Text_Şifre_Kayıt2";
            this.Text_Şifre_Kayıt2.Size = new System.Drawing.Size(100, 20);
            this.Text_Şifre_Kayıt2.TabIndex = 42;
            // 
            // Label_Şifre_Kayıt2
            // 
            this.Label_Şifre_Kayıt2.AutoSize = true;
            this.Label_Şifre_Kayıt2.Location = new System.Drawing.Point(157, 177);
            this.Label_Şifre_Kayıt2.Name = "Label_Şifre_Kayıt2";
            this.Label_Şifre_Kayıt2.Size = new System.Drawing.Size(78, 13);
            this.Label_Şifre_Kayıt2.TabIndex = 41;
            this.Label_Şifre_Kayıt2.Text = "Şifreniz(Tekrar)";
            // 
            // Buton_Kayıt_Tamamla
            // 
            this.Buton_Kayıt_Tamamla.Location = new System.Drawing.Point(243, 200);
            this.Buton_Kayıt_Tamamla.Name = "Buton_Kayıt_Tamamla";
            this.Buton_Kayıt_Tamamla.Size = new System.Drawing.Size(100, 25);
            this.Buton_Kayıt_Tamamla.TabIndex = 43;
            this.Buton_Kayıt_Tamamla.Text = "Tamamla";
            this.Buton_Kayıt_Tamamla.UseVisualStyleBackColor = true;
            this.Buton_Kayıt_Tamamla.Click += new System.EventHandler(this.Buton_Kayıt_Tamamla_Click);
            // 
            // Dosya_Resim_Aç
            // 
            this.Dosya_Resim_Aç.FileName = "Resim_Menüsü";
            // 
            // Buton_Resim_Yükle
            // 
            this.Buton_Resim_Yükle.Location = new System.Drawing.Point(13, 166);
            this.Buton_Resim_Yükle.Name = "Buton_Resim_Yükle";
            this.Buton_Resim_Yükle.Size = new System.Drawing.Size(119, 23);
            this.Buton_Resim_Yükle.TabIndex = 44;
            this.Buton_Resim_Yükle.Text = "Resim Yükle";
            this.Buton_Resim_Yükle.UseVisualStyleBackColor = true;
            this.Buton_Resim_Yükle.Click += new System.EventHandler(this.Buton_Resim_Yükle_Click);
            // 
            // Text_Adres_Kayıt
            // 
            this.Text_Adres_Kayıt.Location = new System.Drawing.Point(243, 68);
            this.Text_Adres_Kayıt.Name = "Text_Adres_Kayıt";
            this.Text_Adres_Kayıt.Size = new System.Drawing.Size(100, 20);
            this.Text_Adres_Kayıt.TabIndex = 46;
            // 
            // Label_Adres_Kayıt
            // 
            this.Label_Adres_Kayıt.AutoSize = true;
            this.Label_Adres_Kayıt.Location = new System.Drawing.Point(157, 72);
            this.Label_Adres_Kayıt.Name = "Label_Adres_Kayıt";
            this.Label_Adres_Kayıt.Size = new System.Drawing.Size(49, 13);
            this.Label_Adres_Kayıt.TabIndex = 45;
            this.Label_Adres_Kayıt.Text = "Adresiniz";
            // 
            // Text_Resim
            // 
            this.Text_Resim.Location = new System.Drawing.Point(13, 196);
            this.Text_Resim.Name = "Text_Resim";
            this.Text_Resim.Size = new System.Drawing.Size(145, 20);
            this.Text_Resim.TabIndex = 47;
            // 
            // Kayıt_Formu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 236);
            this.Controls.Add(this.Text_Resim);
            this.Controls.Add(this.Text_Adres_Kayıt);
            this.Controls.Add(this.Label_Adres_Kayıt);
            this.Controls.Add(this.Buton_Resim_Yükle);
            this.Controls.Add(this.Buton_Kayıt_Tamamla);
            this.Controls.Add(this.Text_Şifre_Kayıt2);
            this.Controls.Add(this.Label_Şifre_Kayıt2);
            this.Controls.Add(this.Text_Şifre_Kayıt);
            this.Controls.Add(this.Label_Şifre_Kayıt);
            this.Controls.Add(this.Text_Eposta_Kayıt);
            this.Controls.Add(this.Text_Nick_Kayıt);
            this.Controls.Add(this.Text_Soyisim_Kayıt);
            this.Controls.Add(this.Text_İsim_Kayıt);
            this.Controls.Add(this.Resim_Kayıt);
            this.Controls.Add(this.Label_Eposta_Kayıt);
            this.Controls.Add(this.Label_Nick_Kayıt);
            this.Controls.Add(this.Label_Soyisim_Kayıt);
            this.Controls.Add(this.Label_İsim_Kayıt);
            this.Name = "Kayıt_Formu";
            this.Text = "Kayıt_Formu";
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Kayıt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Eposta_Kayıt;
        private System.Windows.Forms.Label Label_Nick_Kayıt;
        private System.Windows.Forms.Label Label_Soyisim_Kayıt;
        private System.Windows.Forms.Label Label_İsim_Kayıt;
        private System.Windows.Forms.PictureBox Resim_Kayıt;
        private System.Windows.Forms.TextBox Text_İsim_Kayıt;
        private System.Windows.Forms.TextBox Text_Soyisim_Kayıt;
        private System.Windows.Forms.TextBox Text_Nick_Kayıt;
        private System.Windows.Forms.TextBox Text_Eposta_Kayıt;
        private System.Windows.Forms.TextBox Text_Şifre_Kayıt;
        private System.Windows.Forms.Label Label_Şifre_Kayıt;
        private System.Windows.Forms.TextBox Text_Şifre_Kayıt2;
        private System.Windows.Forms.Label Label_Şifre_Kayıt2;
        private System.Windows.Forms.Button Buton_Kayıt_Tamamla;
        private System.Windows.Forms.OpenFileDialog Dosya_Resim_Aç;
        private System.Windows.Forms.Button Buton_Resim_Yükle;
        private System.Windows.Forms.TextBox Text_Adres_Kayıt;
        private System.Windows.Forms.Label Label_Adres_Kayıt;
        private System.Windows.Forms.TextBox Text_Resim;
    }
}