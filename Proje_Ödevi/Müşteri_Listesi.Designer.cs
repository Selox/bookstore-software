﻿namespace Proje_Ödevi
{
    partial class Müsteri_Listesi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.List_Müşteri = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.username = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.email = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.confirmed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // List_Müşteri
            // 
            this.List_Müşteri.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.username,
            this.name,
            this.email,
            this.confirmed});
            this.List_Müşteri.Location = new System.Drawing.Point(12, 12);
            this.List_Müşteri.Name = "List_Müşteri";
            this.List_Müşteri.Size = new System.Drawing.Size(430, 285);
            this.List_Müşteri.TabIndex = 0;
            this.List_Müşteri.UseCompatibleStateImageBehavior = false;
            this.List_Müşteri.View = System.Windows.Forms.View.Details;
            // 
            // id
            // 
            this.id.Text = "ID";
            this.id.Width = 53;
            // 
            // username
            // 
            this.username.Text = "Kullanıcı Adı";
            this.username.Width = 92;
            // 
            // name
            // 
            this.name.Text = "İsim";
            this.name.Width = 81;
            // 
            // email
            // 
            this.email.Text = "Eposta";
            this.email.Width = 106;
            // 
            // confirmed
            // 
            this.confirmed.Text = "Onay Durumu";
            this.confirmed.Width = 119;
            // 
            // Müsteri_Listesi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 309);
            this.Controls.Add(this.List_Müşteri);
            this.Name = "Müsteri_Listesi";
            this.Text = "Müşteri_Listesi";
            this.Load += new System.EventHandler(this.musteriListeForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView List_Müşteri;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.ColumnHeader username;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader email;
        private System.Windows.Forms.ColumnHeader confirmed;
    }
}