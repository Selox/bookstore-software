﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft;
using System.IO;

namespace Proje_Ödevi
{
    /**
      * \brief Kütüphane Sisteminin başlatıcı formu.
      * Bütün işlemler buradan başlar.
      * @param customer müşteri bilgilerini tutan parametre.
      */
    public partial class Kütüphane_Sistemi : Form
    {
        Customer C = new Customer();
        AdminUser A = new AdminUser();
        List<Customer> Custom;
        List<AdminUser> Admins;

        public Kütüphane_Sistemi()
        {
            InitializeComponent();
            Resim_Müşteri = new PictureBox();
            Resim_Admin = new PictureBox();
            Resim_Müşteri.SizeMode = PictureBoxSizeMode.StretchImage;
            Resim_Admin.SizeMode = PictureBoxSizeMode.StretchImage;
            Yeni_Üyeler();
            Yükle_Üyeler(false);
            Custom = C.LoadUp();
            Yükle_Üyeler(true);
            Admins = A.LoadUp();
        }

        private void Buton_Müşteri_Giriş_Click(object sender, EventArgs e)
        {
            Giriş_Ekranı G = new Giriş_Ekranı(false, Custom, Admins,C);
            G.Show();

        }

        private void Buton_Admin_Giriş_Click(object sender, EventArgs e)
        {
            Giriş_Ekranı G = new Giriş_Ekranı(true, Custom, Admins,C);
            G.Show();
        }

        private void Kaydet_Üyeler()
        {
            string kaydedilecek = String.Empty;
            Custom = C.LoadUp();
            kaydedilecek = C.listThemAll(",");
            File.WriteAllText(@".\customer.txt", kaydedilecek);
            Admins = A.LoadUp();
            kaydedilecek = A.AddminThemAll(",");
            File.WriteAllText(@".\admin.txt", kaydedilecek);
        }

        private void Yükle_Üyeler(bool admin)
        {
            string Path = "";
            if (!admin)
            {
                Path = @".\customer.txt";
            }
            else
            {
                Path = @".\admin.txt";
            }
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        int id = Int32.Parse(elements[0]);
                        string name = elements[1];
                        string Adress = elements[2];
                        string username = elements[3];
                        string email = elements[4];
                        int Password = Int32.Parse(elements[5]);
                        bool Confirmation = false;
                        if(admin != true)
                        {
                            Confirmation = bool.Parse(elements[6]);
                        }
                        if (admin == true)
                        {
                            A.addAdmin(id, name, Adress, username, email, Password);
                            //Bu fonksiyon admin penceresinden eklemek için kullanılıyor temelde!!!
                        }
                        else if (admin == false && Confirmation == true)
                        {
                            C.addCustomer(id, name, Adress, username, email, Password,Confirmation);
                        }
                    }
                }
            }
        }

        private void Yeni_Üyeler()
        {
            string Path = "";
            Path = @".\NewCustomers.txt";
            string okuma = "";
            char coma = ',';
            string yazma = "";
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        int id = Int32.Parse(elements[0]);
                        string name = elements[1];
                        string Adress = elements[2];
                        string username = elements[3];
                        string email = elements[4];
                        int Password = Int32.Parse(elements[5]);
                        bool Confirmation = bool.Parse(elements[6]);
                        if (Confirmation == true)
                        {
                            C.addCustomer(id, name, Adress, username, email, Password, Confirmation);
                        }
                        else
                        {
                            yazma = lines[i] + "\n";
                        }
                    }
                    File.WriteAllText(@".\NewCustomers.txt", yazma);
                }
            }
        }

        private void Buton_Çıkış_Click(object sender, EventArgs e)
        {
            Kaydet_Üyeler();
            this.Close();
            this.Enabled = false;
        }
    }
}
