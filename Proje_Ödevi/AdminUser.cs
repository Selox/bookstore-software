﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Proje_Ödevi
{
    /** 
     * \brief AdminUser sınıfı.
     *  Sisteme giriş yapacak adminlerin bilgilerini tutan ve arkaplanda bazı fonksiyonalrın çalışmasını sağlayan sınıftır.
     *  @param CustomerID kişi id'sini tutan parametre
     *  @param Name kişi isminin tutan parametre
     *  @param Adress kişi adresini tutan parametre
     *  @param Username kişi adresini tutan parametre
     *  @param Email kişi mailini tutan parametre
     *  @param Password kişi şifresini tutan parametre
     *  @param confirmed kişinin hesabının onaylanıp onaylanmadığını tutan tutan parametre.
     *  @param IsAdmin kişinin admin olup olmadığını tutan parametre tutan parametre.
     *  @param List<Book> kitapların  oluşturulduğu ve saklandığı liste.
     *  @param List<Magazine> dergilerin  oluşturulduğu ve saklandığı liste.
     *  @param List<MusicCD> müziklerin  oluşturulduğu ve saklandığı liste.
     *  @param List<AdminUser> Adminlerin oluşturulduğu ve saklandığı liste.
   */
    public class AdminUser
    {
        public int CustomerID;
        public string Name;
        public string Adress;
        public string Username;
        public string Email;
        public int Password;
        public bool confirmed;
        bool IsAdmin;
        Customer c;
        List<Customer> Customers_List;
        List<AdminUser> Admins_List = new List<AdminUser>();
        List<Book> Books;
        List<Magazine> Magazines;
        List<MusicCD> MusicCDs;
        /**
      * Admin listesine admin ekleyen metod.
      * @see addAdmin(int id, string name, string  adress, string username,string  email,int password)
      */
        public void addAdmin(int id, string name, string  adress, string username,string  email,int password)
        {
            Admins_List.Add(new AdminUser
            {
                CustomerID = id,
                Name = name,
                Adress = adress,
                Username = username,
                Email = email,
                Password = password
            });
        }
        /**
      * Onaylanmamış müşterilerin onaylanması için kullanılan formun çağrı metodu.
      * @see addCustomer()
      */
        public void addCustomer()
        {
            Admin_Onay onay = new Admin_Onay();
            onay.Show();
        }
        /**
      * Yerel dosyadan müşterileri çekip sisteme yükleyen metod.
      * @see LoadCustomers()
      */
        private void LoadCustomers()
        {
            string Path = "";
            Path = @".\customer.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        if (elements[6] == "true")
                        {
                            Customers_List.Add(new Customer
                            {
                                CustomerID = Int32.Parse(elements[0]),
                                Name = elements[1],
                                Adress = elements[2],
                                Username = elements[3],
                                Email = elements[4],
                                Password = Int32.Parse(elements[5]),
                                Exist = bool.Parse(elements[6])
                            });
                        }
                    }
                }
            }
        }
        /**
      * Admin listesindeki adminleri bir stringe atayan metod.
      * @see AddminThemAll(string x)
      * @param x admin bilgilerini içinde tutan parametre.
      */
        public string AddminThemAll(string x)
        {
            string res = "";

            for (int i = 0; i < Admins_List.Count; i++)
            {
                res += Admins_List[i].CustomerID.ToString() + x;
                res += Admins_List[i].Name + x;
                res += Admins_List[i].Adress + x;
                res += Admins_List[i].Username + x;
                res += Admins_List[i].Email + x;
                res += Admins_List[i].Password + x;

                res += Environment.NewLine;
            }
            return res;
        }
        /**
      * Admin listesini döndüren metod.
      * @see List<AdminUser> LoadUp()
      */
        public List<AdminUser> LoadUp()
        {
            return Admins_List;
        }
        /**
      * Admin bilgilerini yazdırmak için kullanılan ön metod.
      * @see addProduct(ItemtoPurchase Item)
      * @param bilgi bilgilerine bakılacak adminin liste indeksini tutan parametre.
      */
        public string AdminInfoTaker(int bilgi)
        {
            string informations = "";
            informations += Admins_List[bilgi].Name;
            informations += Admins_List[bilgi].Adress;
            informations += Admins_List[bilgi].Username;
            informations += Admins_List[bilgi].Email;
            return informations;
        }
        /**
      * Ürünler listesine kitap ekleyen metod.
      * @see addNewBook()
      */
        public void addNewBook()
        {
            Ürün_Ekleme Book = new Ürün_Ekleme("kitap");
            Book.Show();
            
        }
        /**
      * Ürünler listesine dergi ekleyen metod.
      * @see addNewBook()
      */
        public void addNewMagazine()
        {
            Ürün_Ekleme Magazine = new Ürün_Ekleme("dergi");
            Magazine.Show();
           
        }
        /**
      * Ürünler listesine müzik cd'si ekleyen metod.
      * @see addNewBook()
      */
        public void addNewMusicCD()
        {
            Ürün_Ekleme Music = new Ürün_Ekleme("müzik");
            Music.Show();
           
        }
    }
}
