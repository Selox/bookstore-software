﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proje_Ödevi
{
    public partial class Ödeme_Paneli : Form
    {
        public Ödeme_Paneli()
        {
            InitializeComponent();
        }

        private void Buton_Nakit_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void Buton_KrediKart_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void Buton_BankaKart_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void Buton_Bitcoin_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
