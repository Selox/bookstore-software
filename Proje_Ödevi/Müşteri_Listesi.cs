﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Proje_Ödevi
{
    /** 
     * \brief Müşteri Listesi formu.
     *  Adminlerin müşteri listesini görmek için çağırdığı formdur.
   */
    public partial class Müsteri_Listesi : Form
    {
        List<Customer> customerList;
        List<Product> productlist = new List<Product>();
        public Müsteri_Listesi(List<Customer> Customer_List)
        {
            InitializeComponent();
            if (Customer_List != null)
            {
                customerList = Customer_List;
            }
            else
            {
                Yükle_Ürünler();
                Müsteri_Listesi.ActiveForm.Width = 358;
                id.Text = "";
                username.Text = "Tür";
                name.Text = "Ürün Adı";
                email.Text = "Fiyat";
                List_Müşteri.Width = 318;
                confirmed.ListView.Visible = false;
                this.Close();
            }
        }

        private void musteriListeForm_Load(object sender, EventArgs e)
        {
            string Path = "";
            Path = @".\customer.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);

                        String[] listItem = { elements[0], elements[1], elements[3], elements[4], elements[6] };
                        ListViewItem listViewItem = new ListViewItem(listItem);
                        List_Müşteri.Items.Add(listViewItem);
                    }
                }
            }
            Path = "";
            Path = @".\NewCustomers.txt";
            okuma = "";
            i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        String[] listItem = { elements[0], elements[1], elements[3], elements[4], elements[6] };
                        ListViewItem listViewItem = new ListViewItem(listItem);
                        List_Müşteri.Items.Add(listViewItem);
                    }
                }
            }
        }

        private void Yükle_Ürünler()
        {
            string Path = "";
            Path = @".\product.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        if (elements[0] == "Book")
                        {
                            String[] listItem = { elements[1], elements[0], elements[2], elements[3]  };
                            ListViewItem listViewItem = new ListViewItem(listItem);
                            List_Müşteri.Items.Add(listViewItem);
                        }
                        else if (elements[0] == "Magazine")
                        {
                            String[] listItem = { elements[1], elements[0], elements[2], elements[3]  };
                            ListViewItem listViewItem = new ListViewItem(listItem);
                            List_Müşteri.Items.Add(listViewItem);
                        }
                        else
                        {
                            String[] listItem = { elements[1], elements[0], elements[2], elements[3]  };
                            ListViewItem listViewItem = new ListViewItem(listItem);
                            List_Müşteri.Items.Add(listViewItem);
                        }
                    }

                }
            }
        }
    }
}
