﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Proje_Ödevi
{
    public partial class Admin_Onay : Form
    {
        public Admin_Onay()
        {
            InitializeComponent();
            string Path = "";
            Path = @".\NewCustomers.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        string[] row = new string[] { elements[0], elements[1], elements[2], elements[3], elements[4], elements[6], elements[5] };
                        Data_Onaylanmamışlar.Rows.Add(row);
                    }
                }
            }
            
        }
        int rowIndex;
        private void Buton_Onayla_Click(object sender, EventArgs e)
        {
            Data_Onaylanmamışlar.Rows[this.rowIndex].Cells["Confirmation"].Value = true;
            rowIndex = 0;
            Data_Onaylanmamışlar.ClearSelection();
        }

        private void Buton_Kaydet_Click(object sender, EventArgs e)
        {
            int i = 0;
            string res = "";
            for (i = 0; i < Data_Onaylanmamışlar.Rows.Count-1; i++)
            {
                res += (int.Parse(Data_Onaylanmamışlar.Rows[i].Cells["id"].Value.ToString())).ToString() + ",";
                res += Data_Onaylanmamışlar.Rows[i].Cells["_Name"].Value.ToString() + ",";
                res += Data_Onaylanmamışlar.Rows[i].Cells["Adress"].Value.ToString() + ",";
                res += Data_Onaylanmamışlar.Rows[i].Cells["Username"].Value.ToString() + ",";
                res += Data_Onaylanmamışlar.Rows[i].Cells["Email"].Value.ToString() + ",";
                res += (int.Parse(Data_Onaylanmamışlar.Rows[i].Cells["Password"].Value.ToString())).ToString() + ",";
                res += (bool.Parse(Data_Onaylanmamışlar.Rows[i].Cells["Confirmation"].Value.ToString())).ToString() + ",";
                res += Environment.NewLine;
            }
            File.WriteAllText(@".\NewCustomers.txt", res);
            this.Close();
        }

        private void Data_Onaylanmamışlar_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
            {
                this.Data_Onaylanmamışlar.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.Data_Onaylanmamışlar.CurrentCell = this.Data_Onaylanmamışlar.Rows[e.RowIndex].Cells[1];
            }
        }
    }
}
