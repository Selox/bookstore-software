﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Proje_Ödevi
{
    /** 
    * \brief Giriş Ekranı formu.
    *  Sisteme giriş yapılması için girilen kişi bilgilerini kıyaslayan formdur.
  */
    public partial class Giriş_Ekranı : Form
    {
        Customer müşteri;
        AdminUser yönetici;
        List<Customer> müşteri_Liste;
        List<AdminUser> admin_Liste;
        bool admin;

        public Giriş_Ekranı(bool kişi, List<Customer> Customers, List<AdminUser> Admin,Customer customer)
        {
            InitializeComponent();
            admin = kişi;
            admin_Liste = Admin;
            müşteri_Liste = Customers;
            müşteri = customer;
            if(kişi == true)
            {
                Buton_Kayıt.Visible = false;
            }
            else
            {
                Buton_Kayıt.Visible = true;
            }
        }

        private Customer checkCustomer(string nick, int password, List<Customer> müşteri_Liste)
        {
            int i = 0;
            bool flag = false;
            if (müşteri_Liste.Count == 0) {   return null;   }
            else
            {
                for (i = 0; i < müşteri_Liste.Count; i++) 
                {
                    if (nick == müşteri_Liste[i].Username)
                    {
                        if (password == müşteri_Liste[i].Password)
                        {
                            flag = true;
                            return müşteri_Liste[i];
                        }
                    }
                    else
                    {
                        continue;
                    }

                }
                if (flag == true) { return müşteri_Liste[i]; }
                else { return null; }
            }
        }

        private AdminUser checkAdmin(string nick, int password, List<AdminUser> admin_Liste)
        {
            int i = 0;
            bool flag = false;
            if (admin_Liste.Count == 0) { return null; }
            else
            {
                for (; i < admin_Liste.Count; i++)
                {
                    if (nick == admin_Liste[i].Username)
                    {
                        if(password == admin_Liste[i].Password)
                        {
                            flag = true;
                            return admin_Liste[i];
                        }
                    }
                    else
                    {
                        continue;
                    }

                }
                if (flag == true) { return admin_Liste[i]; }
                else { return null; }
            }
        }

        private void Buton_Giriş_Click(object sender, EventArgs e)
        {
            string Username = "";
            int Password = 0;
            Username = Text_TakmaAd.Text;
            if(Text_Şifre.Text != "")
            {
                Password = Int32.Parse(Text_Şifre.Text);
            }
            if(admin == true)
            {
                yönetici = checkAdmin(Username, Password, admin_Liste);
            }
            else
            {
                müşteri = checkCustomer(Username, Password, müşteri_Liste);
            }
            if(yönetici != null && admin == true)
            {
                Admin_Formu Admin = new Admin_Formu(yönetici,müşteri_Liste);
                Admin.Show();
                this.Close();
            }
            else if (müşteri != null && admin == false)
            {
                Müşteri_Formu customer = new Müşteri_Formu(müşteri);
                customer.Show();
                this.Close();

            }
            else
            {
                MessageBox.Show("Lütfen Gerekli Alanları Doldurunuz!", "Hatalı Giriş!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Buton_Kayıt_Click(object sender, EventArgs e)
        {
            Kayıt_Formu kayıt = new Kayıt_Formu();
            kayıt.Show();
            this.Close();
        }
    }
}
