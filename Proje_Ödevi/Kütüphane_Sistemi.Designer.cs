﻿namespace Proje_Ödevi
{
    partial class Kütüphane_Sistemi
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_HosGel = new System.Windows.Forms.Label();
            this.Label_A_Giriş = new System.Windows.Forms.Label();
            this.Label_M_Giriş = new System.Windows.Forms.Label();
            this.Buton_Müşteri_Giriş = new System.Windows.Forms.Button();
            this.Buton_Admin_Giriş = new System.Windows.Forms.Button();
            this.Resim_Admin = new System.Windows.Forms.PictureBox();
            this.Resim_Müşteri = new System.Windows.Forms.PictureBox();
            this.Buton_Çıkış = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Admin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Müşteri)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_HosGel
            // 
            this.Label_HosGel.AutoSize = true;
            this.Label_HosGel.Location = new System.Drawing.Point(341, 52);
            this.Label_HosGel.Name = "Label_HosGel";
            this.Label_HosGel.Size = new System.Drawing.Size(66, 13);
            this.Label_HosGel.TabIndex = 0;
            this.Label_HosGel.Text = "Hoş Geldiniz";
            // 
            // Label_A_Giriş
            // 
            this.Label_A_Giriş.AutoSize = true;
            this.Label_A_Giriş.Location = new System.Drawing.Point(461, 297);
            this.Label_A_Giriş.Name = "Label_A_Giriş";
            this.Label_A_Giriş.Size = new System.Drawing.Size(61, 13);
            this.Label_A_Giriş.TabIndex = 3;
            this.Label_A_Giriş.Text = "Admin Girişi";
            // 
            // Label_M_Giriş
            // 
            this.Label_M_Giriş.AutoSize = true;
            this.Label_M_Giriş.Location = new System.Drawing.Point(212, 297);
            this.Label_M_Giriş.Name = "Label_M_Giriş";
            this.Label_M_Giriş.Size = new System.Drawing.Size(66, 13);
            this.Label_M_Giriş.TabIndex = 4;
            this.Label_M_Giriş.Text = "Müşteri Girişi";
            // 
            // Buton_Müşteri_Giriş
            // 
            this.Buton_Müşteri_Giriş.Location = new System.Drawing.Point(213, 313);
            this.Buton_Müşteri_Giriş.Name = "Buton_Müşteri_Giriş";
            this.Buton_Müşteri_Giriş.Size = new System.Drawing.Size(63, 23);
            this.Buton_Müşteri_Giriş.TabIndex = 5;
            this.Buton_Müşteri_Giriş.Text = "Giriş";
            this.Buton_Müşteri_Giriş.UseVisualStyleBackColor = true;
            this.Buton_Müşteri_Giriş.Click += new System.EventHandler(this.Buton_Müşteri_Giriş_Click);
            // 
            // Buton_Admin_Giriş
            // 
            this.Buton_Admin_Giriş.Location = new System.Drawing.Point(461, 313);
            this.Buton_Admin_Giriş.Name = "Buton_Admin_Giriş";
            this.Buton_Admin_Giriş.Size = new System.Drawing.Size(63, 23);
            this.Buton_Admin_Giriş.TabIndex = 6;
            this.Buton_Admin_Giriş.Text = "Giriş";
            this.Buton_Admin_Giriş.UseVisualStyleBackColor = true;
            this.Buton_Admin_Giriş.Click += new System.EventHandler(this.Buton_Admin_Giriş_Click);
            // 
            // Resim_Admin
            // 
            this.Resim_Admin.ImageLocation = @".\Admin.jpg";
            this.Resim_Admin.Location = new System.Drawing.Point(402, 84);
            this.Resim_Admin.Name = "Resim_Admin";
            this.Resim_Admin.Size = new System.Drawing.Size(186, 200);
            this.Resim_Admin.TabIndex = 2;
            this.Resim_Admin.TabStop = false;
            // 
            // Resim_Müşteri
            // 
            this.Resim_Müşteri.ImageLocation = @".\Müşteri.jpg";
            this.Resim_Müşteri.Location = new System.Drawing.Point(159, 84);
            this.Resim_Müşteri.Name = "Resim_Müşteri";
            this.Resim_Müşteri.Size = new System.Drawing.Size(186, 200);
            this.Resim_Müşteri.TabIndex = 1;
            this.Resim_Müşteri.TabStop = false;
            // 
            // Buton_Çıkış
            // 
            this.Buton_Çıkış.Location = new System.Drawing.Point(340, 361);
            this.Buton_Çıkış.Name = "Buton_Çıkış";
            this.Buton_Çıkış.Size = new System.Drawing.Size(75, 23);
            this.Buton_Çıkış.TabIndex = 7;
            this.Buton_Çıkış.Text = "Çıkış Yap";
            this.Buton_Çıkış.UseVisualStyleBackColor = true;
            this.Buton_Çıkış.Click += new System.EventHandler(this.Buton_Çıkış_Click);
            // 
            // Kütüphane_Sistemi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Buton_Çıkış);
            this.Controls.Add(this.Buton_Admin_Giriş);
            this.Controls.Add(this.Buton_Müşteri_Giriş);
            this.Controls.Add(this.Label_M_Giriş);
            this.Controls.Add(this.Label_A_Giriş);
            this.Controls.Add(this.Resim_Admin);
            this.Controls.Add(this.Resim_Müşteri);
            this.Controls.Add(this.Label_HosGel);
            this.Name = "Kütüphane_Sistemi";
            this.Text = "Kütüphane_Sistem";
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Admin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resim_Müşteri)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_HosGel;
        private System.Windows.Forms.PictureBox Resim_Müşteri;
        private System.Windows.Forms.PictureBox Resim_Admin;
        private System.Windows.Forms.Label Label_A_Giriş;
        private System.Windows.Forms.Label Label_M_Giriş;
        private System.Windows.Forms.Button Buton_Müşteri_Giriş;
        private System.Windows.Forms.Button Buton_Admin_Giriş;
        private System.Windows.Forms.Button Buton_Çıkış;
    }
}

