﻿namespace Proje_Ödevi
{
    partial class Ödeme_Paneli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Buton_Nakit = new System.Windows.Forms.Button();
            this.Buton_KrediKart = new System.Windows.Forms.Button();
            this.Buton_BankaKart = new System.Windows.Forms.Button();
            this.Buton_Bitcoin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lütfen bir ödeme türü seçin";
            // 
            // Buton_Nakit
            // 
            this.Buton_Nakit.Location = new System.Drawing.Point(12, 48);
            this.Buton_Nakit.Name = "Buton_Nakit";
            this.Buton_Nakit.Size = new System.Drawing.Size(91, 23);
            this.Buton_Nakit.TabIndex = 1;
            this.Buton_Nakit.Text = "Kapıda Ödeme";
            this.Buton_Nakit.UseVisualStyleBackColor = true;
            this.Buton_Nakit.Click += new System.EventHandler(this.Buton_Nakit_Click);
            // 
            // Buton_KrediKart
            // 
            this.Buton_KrediKart.Location = new System.Drawing.Point(109, 48);
            this.Buton_KrediKart.Name = "Buton_KrediKart";
            this.Buton_KrediKart.Size = new System.Drawing.Size(75, 23);
            this.Buton_KrediKart.TabIndex = 2;
            this.Buton_KrediKart.Text = "Kredi Kartı";
            this.Buton_KrediKart.UseVisualStyleBackColor = true;
            this.Buton_KrediKart.Click += new System.EventHandler(this.Buton_KrediKart_Click);
            // 
            // Buton_BankaKart
            // 
            this.Buton_BankaKart.Location = new System.Drawing.Point(190, 48);
            this.Buton_BankaKart.Name = "Buton_BankaKart";
            this.Buton_BankaKart.Size = new System.Drawing.Size(75, 23);
            this.Buton_BankaKart.TabIndex = 3;
            this.Buton_BankaKart.Text = "Banka Kartı";
            this.Buton_BankaKart.UseVisualStyleBackColor = true;
            this.Buton_BankaKart.Click += new System.EventHandler(this.Buton_BankaKart_Click);
            // 
            // Buton_Bitcoin
            // 
            this.Buton_Bitcoin.Location = new System.Drawing.Point(271, 48);
            this.Buton_Bitcoin.Name = "Buton_Bitcoin";
            this.Buton_Bitcoin.Size = new System.Drawing.Size(75, 23);
            this.Buton_Bitcoin.TabIndex = 4;
            this.Buton_Bitcoin.Text = "bitcoin";
            this.Buton_Bitcoin.UseVisualStyleBackColor = true;
            this.Buton_Bitcoin.Click += new System.EventHandler(this.Buton_Bitcoin_Click);
            // 
            // Ödeme_Paneli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 111);
            this.Controls.Add(this.Buton_Bitcoin);
            this.Controls.Add(this.Buton_BankaKart);
            this.Controls.Add(this.Buton_KrediKart);
            this.Controls.Add(this.Buton_Nakit);
            this.Controls.Add(this.label1);
            this.Name = "Ödeme_Paneli";
            this.Text = "Ödeme_Paneli";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Buton_Nakit;
        private System.Windows.Forms.Button Buton_KrediKart;
        private System.Windows.Forms.Button Buton_BankaKart;
        private System.Windows.Forms.Button Buton_Bitcoin;
    }
}