﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proje_Ödevi
{
    /** 
    * \brief Müşteri formu.
    *  Müşterilerin kendi bilgilerini görebildikleri ve farklı işlemleri gerçekleştirdikleri arayüz formudur.
  */
    public partial class Müşteri_Formu : Form
    {
        int id;
        Customer Customer;
        public Müşteri_Formu(Customer customer)
        {
            InitializeComponent();
            Customer = customer;
            id = customer.CustomerID;
            Label_Müşteri_İsim.Text = customer.Name;
            Label_Müşteri_Adres.Text = customer.Adress;
            Label_Müşteri_Nick.Text = customer.Username;
            Label_Müşteri_Eposta.Text = customer.Email;
            Resim_Müşteri.ImageLocation = customer.Picture;
            Resim_Müşteri.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void Buton_Alışveriş_Click(object sender, EventArgs e)
        {
            AlışverişSepeti alışveriş = new AlışverişSepeti(id, Customer);
            alışveriş.Show();

        }

        private void Buton_Kaydet_Çık_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void Buton_Bilgileri_Güncelle_Click(object sender, EventArgs e)
        {

        }
    }
}
