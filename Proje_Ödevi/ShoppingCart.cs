﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
namespace Proje_Ödevi
{
    /** 
     * \brief Shoping Cart Sınfı.
     *  Alışverişin yapılabilmesi için kullanılan sınıftır.
   */
    class ShoppingCart
    {
        /**
       * .
       * @param CustomerID müşteri ID sini tutar.
       * @param items satın alınacak eşyaları tutar.
       * @param paymentAmount ödeme miktarını tutar.
       * @param paymentType ödeme türlerinin bilgilerini tutar.
       * @return The test results
       */
        int CustomerID;
        Customer customer;
        List<ItemtoPurchase> items = new List<ItemtoPurchase>();
        ItemtoPurchase itemstoPurchase = new ItemtoPurchase();
        double paymentAmount = 0.0;
        string[] paymentType = { "Nakit", "Banka Kartı", "Kredi Kartı", "Bitcoin", "Kapıda Ödeme" };
        /**
       * Yazdırmayı sağlayan metod.
       * @see printProducts()
       */
        public void printProducts()
        {
            
        }
        /**
      * Alışveriş listesine ürün ekleyen metod.
      * @see addProduct(ItemtoPurchase Item)
      * @param Item eklemesi yapılacak eşyayı tutan parametre.
      * @param yazı eşyanın yazdıralacak bilgilerini tutan parametre.
      */
        public void addProduct(ItemtoPurchase Item)
        {
            string yazı = "";
            yazı = Item.id + " | " + Item.name + " | " + Item.price + "TL | " + Item.quantity + " Adet";
            items.Add(new ItemtoPurchase { id = Item.id, name = Item.name, price = Item.price, quantity = 1 });
        }
        /**
      * Alışveriş listesinden ürün çıkaran metod.
      * @see removeProduct(ItemtoPurchase Item)
      * @param Item çıkarması yapılacak eşyayı tutan parametre.
      * @param i döngü için hazırlanmış parametre.
      */
        public void removeProduct(ItemtoPurchase Item)
        {
            int i = 0;
            for (i = 0; i < items.Count; i++)
            {
                if (items[i].id.ToString() == Item.id.ToString())
                {
                    break;
                }
            }
            items.RemoveAt(i);
        }
        /**
      * Alışveriş listesindeki ürünleri satın alma metodu.
      * @see placeOrder(Customer customer)
      * @param customer müşteri bilgilerini tutan parametre.
      * @param i döngü için hazırlanmış parametre.
      * @param ödeme ödeme panelini açacak olan sınıf parametresi.
      * 
      */
        public double placeOrder(Customer customer)
        {
            int i = 0;
            for (i = 0; i < items.Count; i++)
            {
                paymentAmount += (items[i].price * items[i].quantity);
            }
            Ödeme_Paneli ödeme = new Ödeme_Paneli();
            ödeme.Show();
            sendInvoicebyEmail(customer, true);
            return paymentAmount;
        }
        /**
      * Alışveriş listesindeki ürünleri satın alma iptal eden metod.
      * true veya false değer döndürerek çalışır.
      * @see cancelOrder(Customer customer)
      * @param customer müşteri bilgilerini tutan parametre.
      */
        public bool cancelOrder(Customer customer)
        {
            sendInvoicebyEmail(customer, false);
            return false;
        }
        /**
      * Alışveriş listesindeki ürünleri satın alma iptal eden metod.
      * true veya false değer döndürerek çalışır.
      * @see placeOrder(Customer customer)
      * @param customer müşteri bilgilerini tutan parametre.
      */
        private void sendInvoicebyEmail(Customer kişi, bool durum)
        {
            int i = 0;
            if (durum == true)
            {
                string musteriMail = kişi.Email;
                string subject = "Selçuğun Kitap Dükkanı";
                string content = "Bizden alışveriş yaptığınız için teşekkür ederiz\n Satın aldıklarınız:";
                for (i = 0; i < items.Count; i++)
                {
                    content +=
                        "\n" + items[i].name.Trim() + " x " + items[i].quantity;
                }
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("Ooocrazyselcuk@gmail.com");
                mail.To.Add(musteriMail);
                mail.Subject = subject;
                mail.Body = content;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("Selocanus Hopus", "HopusHopus26");
                SmtpServer.EnableSsl = true;


            }
            else
            {
                Console.WriteLine("Your purchase is cancelled. Thank you :)");
            }
        }
        /**
      * Alışveriş listesindeki ürünleri döndüren metod.
      * Satın alınacak eşyaları döndürür.
      * @see  List<ItemtoPurchase> LoadUp()
      */
        public List<ItemtoPurchase> LoadUp()
        {
            return items;
        }

    }
}
