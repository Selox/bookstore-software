﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Proje_Ödevi
{
    public partial class Ürün_Ekleme : Form
    {
        List<Book> Books = new List<Book>();
        List<Magazine> Magazines = new List<Magazine>();
        List<MusicCD> MusicCDs = new List<MusicCD>();
        string tip;
        string Filename;
        public Ürün_Ekleme(string ürün)
        {
            InitializeComponent();
            tip = ürün;
            if(ürün == "kitap")
            {
                Label_SayfaSayısı_Ürün.Visible = true;
                Text_Sayfa_Ürün.Visible = true;
                Label_Yayıncı_Ürün.Visible = true;
                Text_Yayıncı_Ürün.Visible = true;
                Label_Tür_Ürün.Text = "Yazar";
            }
            else if (ürün == "dergi")
            {
                Label_Tanım_Ürün.Text = "Cilt";
            }
            else
            {
                Label_Tanım_Ürün.Text = "Şarkıcı";
            }
            Yükle_Ürünler();

        }

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        private void Buton_Kayıt_Tamamla_Click(object sender, EventArgs e)
        {
            int ide = RandomNumber(1, 1000);
            if (tip == "kitap")
            {
                Books.Add(new Book { id = ide, name = Text_İsim_Ürün.Text, price = Int32.Parse(Text_Fiyat_Ürün.Text), ISBN = Int32.Parse(Text_Tanım_Ürün.Text), author = Text_Tür_Ürün.Text, publisher = Text_Yayıncı_Ürün.Text, page = Int32.Parse(Text_Sayfa_Ürün.Text) , cover_page_picture = Filename });
            }
            else if(tip == "dergi")
            {
                Magazines.Add(new Magazine { id = ide, name = Text_İsim_Ürün.Text, price = Int32.Parse(Text_Fiyat_Ürün.Text), issue = Int32.Parse(Text_Tanım_Ürün.Text), type = Text_Tür_Ürün.Text , picture = Filename });
            }
            else
            {
                MusicCDs.Add(new MusicCD { id = ide, name = Text_İsim_Ürün.Text, price = Int32.Parse(Text_Fiyat_Ürün.Text), singer = Text_Tanım_Ürün.Text, type = Text_Tür_Ürün.Text , picture = Filename });
            }
            Kaydet_Ürünler();
            this.Close();
        }

        private void Buton_Resim_Yükle_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Dosya Açma";
            open.Filter = "Image Files(*.jpg; *.jpeg; *.bmp)|*.jpg; *.jpeg; *.bmp";
            open.InitialDirectory = @"d:\";
            if (open.ShowDialog() == DialogResult.OK)
            {
                Text_Resim.Text = open.FileName;
                Resim_Kayıt.Image = new Bitmap(open.FileName);
                Resim_Kayıt.SizeMode = PictureBoxSizeMode.StretchImage;
                Filename = open.FileName;
            }

        }

        private void Yükle_Ürünler()
        {
            string Path = "";
            Path = @".\product.txt";
            string okuma = "";
            char coma = ',';
            int i = 0;
            if (File.Exists(Path))
            {
                StreamReader streamReader = new StreamReader(Path);
                okuma += streamReader.ReadToEnd();
                streamReader.Close();
                okuma = okuma.Trim();
                if (okuma != "")
                {
                    string[] lines = okuma.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (i = 0; i < lines.Length; i++)
                    {
                        string[] elements = lines[i].Split(coma);
                        if (elements[0] == "Book")
                        {
                            Books.Add(new Book { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]), ISBN = Int32.Parse(elements[4]), author = elements[5], publisher = elements[6], page = Int32.Parse(elements[7]),cover_page_picture= elements[8] });
                        }
                        else if (elements[0] == "Magazine")
                        {
                            Magazines.Add(new Magazine { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]), issue = Int32.Parse(elements[4]), type = elements[5] , picture = elements[6] });
                        }
                        else
                        {
                            MusicCDs.Add(new MusicCD { id = Int32.Parse(elements[1]), name = elements[2], price = Double.Parse(elements[3]), singer = elements[4], type = elements[5] , picture = elements[6] });
                        }
                    }

                }
            }
        }

        private void Kaydet_Ürünler()
        {
            string Path;
            Path = @".\product.txt";
            int i = 0;
            if (File.Exists(Path))
            {
                string res = "";
                for (i = 0; i < Books.Count; i++)
                {
                    res += "Book" + ",";
                    res += Books[i].id + ",";
                    res += Books[i].name + ",";
                    res += Books[i].price.ToString() + ",";
                    res += Books[i].ISBN.ToString() + ",";
                    res += Books[i].author + ",";
                    res += Books[i].publisher + ",";
                    res += Books[i].page.ToString() + ",";
                    res += Books[i].cover_page_picture + ",";
                    res += Environment.NewLine;
                }
                for (i = 0; i < Magazines.Count; i++)
                {
                    res += "Magazine" + ",";
                    res += Magazines[i].id + ",";
                    res += Magazines[i].name + ",";
                    res += Magazines[i].price.ToString() + ",";
                    res += Magazines[i].issue.ToString() + ",";
                    res += Magazines[i].type + ",";
                    res += Magazines[i].picture + ",";
                    res += Environment.NewLine;
                }
                for (i = 0; i < MusicCDs.Count; i++)
                {
                    res += "MusicCD" + ",";
                    res += MusicCDs[i].id + ",";
                    res += MusicCDs[i].name + ",";
                    res += MusicCDs[i].price.ToString() + ",";
                    res += MusicCDs[i].singer + ",";
                    res += MusicCDs[i].type + ",";
                    res += MusicCDs[i].picture + ",";
                    res += Environment.NewLine;
                }
                File.WriteAllText(@".\product.txt", res);
            }
            this.Close();
        }
    }
}
