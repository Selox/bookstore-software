﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proje_Ödevi
{
    /** 
    * \brief Admin Formu.
    *  Sisteme giriş yapan adminlerin farklı işlemler gerçekleştirebilmek için kullandıkları arayüzdür.
  */
    public partial class Admin_Formu : Form
    {
        List<Customer> Customer_List;
        Customer customer = new Customer();
        List<AdminUser> Admin_List;
        AdminUser Admin;
        public Admin_Formu(AdminUser admin, List<Customer> custList)
        {
            InitializeComponent();
            Customer_List = custList;
            Admin_List = admin.LoadUp();
            Admin = admin;
            Label_Admin_İsim.Text = admin.Name;
            Label_Admin_Adres.Text = admin.Adress;
            Label_Admin_Nick.Text = admin.Username;
            Label_Admin_Eposta.Text = admin.Email;
        }

        private void Buton_Müşteri_Göster_Click(object sender, EventArgs e)
        {
            Müsteri_Listesi musteriListeForm = new Müsteri_Listesi(Customer_List);
            musteriListeForm.Show();
        }

        private void Buton_Müşteri_Onay_Click(object sender, EventArgs e)
        {
            Admin.addCustomer();
        }

        private void Buton_Ürün_Göster_Click(object sender, EventArgs e)
        {
            //Müsteri_Listesi musteriListeForm = new Müsteri_Listesi(null);
            //musteriListeForm.Show();
        }

        private void Buton_Kitap_Ekle_Click(object sender, EventArgs e)
        {
            Admin.addNewBook();
        }

        private void Buton_Dergi_Ekle_Click(object sender, EventArgs e)
        {
            Admin.addNewMagazine();
        }

        private void Buton_Müzik_Ekle_Click(object sender, EventArgs e)
        {
            Admin.addNewMusicCD();
        }
    }
}
