﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje_Ödevi
{
    /** 
     * \brief Book sınıfı.
     *  Sistemdeki kitapların oluşturulması için gereken sınıftır.
     *  @param ISBN kitap ISBN bilgisini tutan parametre
     *  @param author yazar isminin tutan parametre
     *  @param publisher yayıncı bilgisini tutan parametre
     *  @param page kitap sayfa sayısının bilgisini tutan parametre
     *  @param cover_page_picture kitap kapak sayfasını tutan parametre
   */
    public class Book : Product
    {
        public int ISBN { get; set; }
        public string author { get; set; }
        public string publisher { get; set; }
        public int page { get; set; }
        public string cover_page_picture { get; set; }
    }
}
